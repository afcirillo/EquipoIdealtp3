package model;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import controls.Conflict;
import controls.Employee;
import controls.Picture;
import controls.Requirement;
import controls.RequirementItem;
import controls.Role;
import controls.Team;

public class DataInterface {
	private Entity<EmployeeRecord> employees;
	private Entity<RoleRecord> roles;
	private Entity<ConflictRecord> conflicts;
	private Entity<RequirementRecord> requirements;
	private Entity<TeamRecord> teams;

	public DataInterface() {

	}

	// EMPLOYEE
	public void addEmployee(Employee employee) {
		EmployeeRecord record = employeeToRecord(employee);
		employees.insert(record);
		employees.commit();
	}

	public void editEmployee(Employee employee) {
		EmployeeRecord record = employeeToRecord(employee);
		employees.update(record);
		employees.commit();
	}

	public void deleteEmployee(Employee employee) {
		EmployeeRecord record = employeeToRecord(employee);
		employees.delete(record);
		employees.commit();
		List<ConflictRecord> conflictsToRemove = conflicts.recordSet().stream()
				.filter(c -> c.employeeId_1() == record.id() || c.employeeId_2() == record.id())
				.collect(Collectors.toList());
		conflictsToRemove.forEach(conflicts::delete);
		conflicts.commit();
	}
	
	private EmployeeRecord employeeToRecord(Employee employee) {
		EmployeeRecord record = new EmployeeRecord();
		
		record.id(employee.id());
		record.name(employee.name());
		record.document(employee.document());
		record.gender(employee.gender());
		record.age(employee.age());
		record.email(employee.email());
		record.phone(employee.phone());
		record.roleId(roleToRecord(employee.role()).id());
		record.photo(getPicture(employee.photo()));
		return record;
	}
	
	private Employee recordToEmployee(EmployeeRecord record) {
		Employee employee = new Employee();
		employee.id(record.id());
		employee.name(record.name());
		employee.document(record.document());
		employee.gender(record.gender());
		employee.age(record.age());
		employee.email(record.email());
		employee.phone(record.phone());
		employee.role(recordToRole(roles.select(record.roleId())));
		
		employee.photo(getImageFromPicture(record.photo()));
		return employee;
	}

	// ROLE
	public void addRole(Role role) {
		roles.insert(roleToRecord(role));
		roles.commit();
	}

	public void editRole(Role role) {
		roles.update(roleToRecord(role));
		roles.commit();
	}

	public void deleteRole(Role role) {
		roles.delete(roleToRecord(role));
		roles.commit();
	}
	
	private RoleRecord roleToRecord(Role role) {
		RoleRecord record = new RoleRecord(role.description());
		record.id(role.id());
		return record;
	}

	private Role recordToRole(RoleRecord record) {
		if (record == null)
			return new Role("Sin especificar");
		Role role = new Role(record.description());
		role.id(record.id());
		return role;
	}
	// CONFLICTS
	public void addConflict(Conflict conflict) {
		conflicts.insert(conflictToRecord(conflict));		
		conflicts.commit();
	}

	public void editConflict(Conflict conflict) {
		conflicts.update(conflictToRecord(conflict));
		conflicts.commit();
	}

	public void deleteConflict(Conflict conflict) {
		conflicts.delete(conflictToRecord(conflict));
		conflicts.commit();
	}
	
	protected ConflictRecord conflictToRecord(Conflict conflict) {
		int employeeId_1 = employeeToRecord(conflict.employee1()).id();
		int employeeId_2 = employeeToRecord(conflict.employee2()).id();
		
		ConflictRecord record = new ConflictRecord(employeeId_1, employeeId_2);
		int id = 0;
		try{
			id = conflicts.recordSet().stream().filter(c -> c.equals(record)).findFirst().get().id();
		}catch(Exception e) {
			id = 0;
		}
		record.id(id);
		return record;
	}
	
	private Conflict recordToConflict(ConflictRecord record) {
		EmployeeRecord employee1 = employees.recordSet().stream().filter(e -> (e.id() == record.employeeId_1())).findFirst().get();
		EmployeeRecord employee2 = employees.recordSet().stream().filter(e -> (e.id() == record.employeeId_2())).findFirst().get();
		return new Conflict(recordToEmployee(employee1), recordToEmployee(employee2));
	}

	public ArrayList<Employee> employees() {
		ArrayList<Employee> list = new ArrayList<Employee>();
		for (EmployeeRecord r: employees.recordSet())
			list.add(recordToEmployee(r));
		return list;
	}

	public void employees(ArrayList<Employee> employees) {
		ArrayList<EmployeeRecord> list = new ArrayList<EmployeeRecord>();
		for (Employee e: employees)
			list.add(employeeToRecord(e));
		this.employees.setRecords(list);
	}

	public ArrayList<Role> roles() {
		ArrayList<Role> list = new ArrayList<Role>();
		for (RoleRecord r: roles.recordSet())
			list.add(recordToRole(r));
		return list;
	}

	public ArrayList<Conflict> conflicts() {
		ArrayList<Conflict> list = new ArrayList<Conflict>();
		for (ConflictRecord r: conflicts.recordSet())
			list.add(recordToConflict(r));
		return list;
	}

	public void conflicts(ArrayList<Conflict> conflicts) {
		ArrayList<ConflictRecord> list = new ArrayList<ConflictRecord>();
		for (Conflict c: conflicts)
			list.add(conflictToRecord(c));
		this.conflicts.setRecords(list);
	}

	// ---------------------------------------------

	public void deleteConflictFrom(Employee employee) {
		EmployeeRecord record = employeeToRecord(employee);
		List<ConflictRecord> conflictsToDelete = conflicts.recordSet().stream()
				.filter(c -> (c.employeeId_1() == record.id() || c.employeeId_2() == record.id()))
				.collect(Collectors.toList());
		
		conflictsToDelete.forEach(conflicts::delete);
		conflicts.commit();
	}
	
	public ArrayList<Employee> getEmployeeWithConflicts() {
		ArrayList<Employee> conflictList = new ArrayList<Employee>();
		for (Conflict conflict : conflicts()) {
			if (!conflictList.contains(conflict.employee1()))
				conflictList.add(conflict.employee1());
			if (!conflictList.contains(conflict.employee2()))
				conflictList.add(conflict.employee2());
		}
	//	conflictList.sort(null);
		return conflictList;
	}

	public void loadEntity() {
		employees = new Entity<EmployeeRecord>("entities/employees.dat");
		roles = new Entity<RoleRecord>("entities/roles.dat");
		conflicts = new Entity<ConflictRecord>("entities/conflicts.dat");
		requirements = new Entity<RequirementRecord>("entities/requirements.dat");
		teams = new Entity<TeamRecord>("entities/teams.dat");
	}

	public void addRequirement(Requirement requirement) {
		RequirementRecord record = requirementToRecord(requirement);
		requirements.insert(record);
		requirements.commit();		
	}

	public void editRequirement(Requirement requirement) {
		requirements.update(requirementToRecord(requirement));
		requirements.commit();		
	}

	public void deleteRequirement(Requirement requirement) {
		requirements.delete(requirementToRecord(requirement));
		requirements.commit();
	}
	
	private RequirementRecord requirementToRecord(Requirement requirement) {
		RequirementRecord record = new RequirementRecord();

		record.id(requirement.id());
		record.description(requirement.description());
		for (RequirementItem item: requirement.items()) {
			record.add(requirementItemToRecord(item));
		}
		return record;
	}
	
	private Requirement recordToRequirement(RequirementRecord record) {
		if (record == null)
			return null;
		
		Requirement requirement =  new Requirement();
		requirement.id(record.id());
		requirement.description(record.description());
		requirement.status(record.statusDescription());
		for (RequirementItemRecord item: record.items())
			requirement.add(recordToRequirementItem(item));
		return requirement;
	}
	
	private RequirementItemRecord requirementItemToRecord(RequirementItem item) {
		return new RequirementItemRecord(roleToRecord(item.role()).id(), item.minimum(), item.maximum());
	}
	
	private RequirementItem recordToRequirementItem(RequirementItemRecord item) {
		Role role = recordToRole(roles.recordSet().stream().filter(r -> r.id() == item.roleId()).findFirst().get());
		return new RequirementItem(role, item.minimum(), item.maximum());
	}

	public ArrayList<Requirement> requirements() {
		ArrayList<Requirement> list = new ArrayList<Requirement>();
		for (RequirementRecord r: requirements.recordSet())
			list.add(recordToRequirement(r));
		return list;
	}

	public ArrayList<Team> teams() {
		ArrayList<Team> teams = new ArrayList<Team>();
		for (TeamRecord record: this.teams.recordSet()) {
			teams.add(recordToTeam(record));
		}
		return teams;
	}
	
	public void addTeam(Team team) {
		TeamRecord record = teamToRecord(team);
		teams.insert(record);
		teams.commit();		
	}

	public void editTeam(Team team) {
		teams.update(teamToRecord(team));
		teams.commit();		
	}

	public void deleteTeam(Team team) {
		teams.delete(teamToRecord(team));
		teams.commit();
	}

	
	public Team recordToTeam(TeamRecord record) {
		if (record == null)
			return null;
		Team team = new Team();
		team.id(record.id());
		team.requirement(recordToRequirement(requirements.select(record.requirementId())));
		team.algorithm(record.algorithm());
		team.correctCombinations(record.correctCombinations());
		team.iterations(record.iterations());
		team.elapsedTimeInMs(record.elapsedTimeInMs());
		for (Integer employeeId :record.employeesId()) {
			team.add(recordToEmployee(employees.select(employeeId)));
		}
		return team;
	}
	
	public TeamRecord teamToRecord(Team team) {
		TeamRecord record = new TeamRecord();
		record.id(team.id());
		record.requirementId(team.requirement().id());
		record.algorithm(team.algorithm());
		record.correctCombinations(team.correctCombinations());
		record.iterations(team.iterations());
		record.elapsedTimeInMs(team.elapsedTimeInMs());
		for (Employee employee: team.employees()) {
			record.add(employee.id());
		}
		
		return record;
	}
	
	public static ImageIcon getImageFromPicture(Picture picture) {
		if (picture == null)
			return null;
		BufferedImage bi = new BufferedImage(picture.width(), picture.height(), BufferedImage.TYPE_INT_RGB);
		bi.setRGB(0, 0, picture.width(), picture.height(), picture.rgb(), 0, picture.width());
		return new ImageIcon(bi);
	}
	
	public static Picture getPicture(Icon icon) {
		if (icon == null)
			return null;
		BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        icon.paintIcon(null, image.getGraphics(), 0, 0);
       int[] rgb = image.getRGB(0, 0, icon.getIconWidth(), icon.getIconHeight(), null, 0, icon.getIconWidth());
		return new Picture(icon.getIconWidth(), icon.getIconHeight(), rgb);
	}
}
