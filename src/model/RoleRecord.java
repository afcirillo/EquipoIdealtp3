package model;

import java.io.Serializable;

public class RoleRecord implements Comparable<RoleRecord>, Serializable, Record<RoleRecord>{
	private int id;
	private String description;

	public RoleRecord(String description) {
		description(description);
	}	
	
	public int id() {return id;	}
	public void id(int id) { this.id = id; } 
	public String description() { return description; }
	public void description(String description) { this.description = description; }

	@Override
	public int compareTo(RoleRecord other) {
		return id()-other.id();
	}
	
	@Override
	public String toString() {
		return description;
	}

//	@Override
//	public int nextSequence() {
//		id(++sequenceId);
//		return sequenceId;
//	}

	@Override
	public void update(RoleRecord rol) {
		description(rol.description());	
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleRecord other = (RoleRecord) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
}
