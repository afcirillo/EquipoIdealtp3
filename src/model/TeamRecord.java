package model;

import java.io.Serializable;
import java.util.ArrayList;

public class TeamRecord implements Serializable, Comparable<TeamRecord>, Record<TeamRecord>{

	private int id;
	private int requirementId;
	private String algorithm;
	private int elapsedTimeInMs;
	private int correctCombinations;
	private int iterations;
	private ArrayList<Integer> employeesId; 
	
	public TeamRecord() {
		employeesId(new ArrayList<Integer>());
	}
	
	public void add(int employeeId) {
		if (exists(employeeId))
			return;
		employeesId.add(employeeId);
	}
	
	private boolean exists(int employeeId) {
		return employeesId.contains(employeeId);
	}
	
	public void remove(int employeeId) {
		employeesId.remove(new Integer(employeeId));
	}

	@Override
	public int id() {
		return id;
	}

	@Override
	public void id(int id) {
		this.id = id;
	}

	@Override
	public void update(TeamRecord team) {
		requirementId(team.requirementId());
		employeesId(team.employeesId());		
		iterations(team.iterations());
		correctCombinations(team.correctCombinations());
		elapsedTimeInMs(team.elapsedTimeInMs());
	}

	@Override
	public int compareTo(TeamRecord other) {
		return id()-other.id();
	}
	public int requirementId() {
		return requirementId;
	}
	public void requirementId(int requirementId) {
		this.requirementId = requirementId;
	}
	public ArrayList<Integer> employeesId() {
		return employeesId;
	}
	public void employeesId(ArrayList<Integer> employeesId) {
		this.employeesId = employeesId;
	}
	
	@Override
	public String toString() {
		return "requirement = " + requirementId + " | employeesId = " + employeesId;
	}

	public String algorithm() {
		return algorithm;
	}

	public void algorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeamRecord other = (TeamRecord) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int elapsedTimeInMs() {
		return elapsedTimeInMs;
	}

	public void elapsedTimeInMs(int elapsedTimeInMs) {
		this.elapsedTimeInMs = elapsedTimeInMs;
	}

	public int correctCombinations() {
		return correctCombinations;
	}

	public void correctCombinations(int correctCombinations) {
		this.correctCombinations = correctCombinations;
	}

	public int iterations() {
		return iterations;
	}

	public void iterations(int iterations) {
		this.iterations = iterations;
	}
	
	
}
