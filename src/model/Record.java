package model;

public interface Record<T> {
	public int id();
	public void id(int id);
	public void update(T record);
}
