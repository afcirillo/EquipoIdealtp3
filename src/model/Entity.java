package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Entity<T extends Record<T>> implements Serializable{
	private String file;
	private Table<T> table;
	public Entity(String file) {
		file(file);
		load();
	}
	
	@SuppressWarnings("unchecked")
	private void load() {
		FileInputStream fis;
		ObjectInputStream in;

		try {
			fis = new FileInputStream(file());
			in = new ObjectInputStream(fis);
			table = (Table<T>) in.readObject();
			in.close();
		} catch (Exception e) {
			table = new Table<T>();
			return;
		} 
	}

	public void insert(T record) {
		table.insert(record);
	}
	
	public void deleteAll() {
		table.deleteAll();
	}
	
	public void insertAll(ArrayList<T> records){
			records.forEach(this::insert);
	}

	public void update(T record) {
		table.update(record);
	}
	
	public void delete(T record) {
		table.delete(record);
	}
	
	public ArrayList<T> recordSet() {
		return table.recordSet();
	}
	public void setRecords(ArrayList <T> records) {
		 table.setRecords(records);
	}
	
	public T select(int id) {
		return table.select(id);
	}
	
	public T row(int index) {
		return table.row(index);
	}

	
	
	public void commit() {
		FileOutputStream fos;
		ObjectOutputStream out;
		try {
			fos = new FileOutputStream(file);
			out = new ObjectOutputStream(fos);
			out.writeObject(table);
			out.close();
		} catch (FileNotFoundException e) {e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();}
	}
	
	public void rollback() {
		load();
	}
	
	public String file() { return file;	}
	public void file(String file) { this.file = file; }

	protected class Table<T extends Record<T>> implements Serializable{
		private int sequenceId;
		private ArrayList<T> recordSet;
		
		public Table() {
			sequenceId = 0;
			recordSet = new ArrayList<T>();
		}
		
		public int recordCount() {
			return recordSet.size();
		}
		
		public void insert(T record) {
			if (recordSet.contains(record))
				return;
			record.id(nextSequence());	
			recordSet.add(record);
		}
		
		public void deleteAll() {
			recordSet.removeAll(null);
		}
		
		public void insertAll(ArrayList<T> records){
				records.forEach(this::insert);
		}

		public void update(T record) {
			T recordToUpdate = find(record.id());
			if (recordToUpdate == null)
				return;
			recordToUpdate.update(record);
		}
		
		public void delete(T record) {
			recordSet.remove(record);
		}
		
		private int nextSequence() {
			return ++sequenceId;
		}
		private T find(int id) {		
			int j = 0, k=recordSet.size()-1;
			int m = (j+k)/2;
			while (j <= k) {
				if (recordSet.get(m).id() == id)
					return recordSet.get(m);
				if (recordSet.get(m).id() < id)
					j = m+1;
				else 
					k = m-1;
				m = (j+k)/2;
			}
			return null;
		}
		public ArrayList<T> recordSet() {
			return recordSet;
		}
		public void setRecords(ArrayList <T> record) {
			this.recordSet = record;
		}	
	
		public T row(int index) {
			validateIndex(index);
			return recordSet.get(index);
		}

		private void validateIndex(int index) {
			if (index < 0 || index >= recordCount())
				throw new IllegalArgumentException("indice invalido");			
		}
		
		public T select(int id) {
			return find(id);
		}
		
	}
}
