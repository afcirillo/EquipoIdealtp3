package model;

import java.io.Serializable;

import controls.Picture;

public class EmployeeRecord implements Record<EmployeeRecord>, Serializable, Comparable<EmployeeRecord>{
//	private static int sequenceId;
	private int id;
	private int document;
	private String name;
	private String gender;
	private int age;
	private String email;
	private String phone;
	private Picture photo;
	private int roleId;
	
	public EmployeeRecord() {
	}
	
	public EmployeeRecord(String name, int document, int roleId) {
		name(name);
		document(document);
		roleId(roleId);
	}
	
//	@Override
//	public int nextSequence() {
//		id(++sequenceId);
//		return sequenceId;
//	}

	@Override
	public int id() {
		return id;
	}

	@Override
	public void update(EmployeeRecord employee) {
		validaEmploye(employee);
		document(employee.document());
		name(employee.name());
		gender(employee.gender());
		age(employee.age());
		email(employee.email());
		phone(employee.phone());
		photo(employee.photo());
		roleId(employee.roleId());
	}
	
	private void validaEmploye(EmployeeRecord employe) {
		if (employe == null)
			throw new IllegalArgumentException("El empleado no puede ser nulo");		
	}
	
	public int document() { 
		return document; 
	}
	
	public void document(int document) { 
		this.document = document; 
	}
	
	public String name() { 
		return name;	
	}
	
	public void name(String name) {	
		this.name = name; 
	}
	
	public String email() {	
		return email; 
	}
	
	public void email(String email) { 
		this.email = email; 
	}
	
	public String phone() { 
		return phone; 
	}
	
	public void phone(String phone) { 
		this.phone = phone; 
	}
	
	public Picture photo() {	
		return photo; 
	}
	
	public void photo(Picture photo) { 
		this.photo = photo; 
	}
	
	public int roleId() { 
		return roleId; 
	}
	
	public void roleId(int roleId) { 
		this.roleId = roleId; 
	}
	
	public String gender() {
		return gender;
	}

	public void gender(String gender) {
		this.gender = gender;
	}

	public int age() {
		return age;
	}

	public void age(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return name();
	}

	@Override
	public int compareTo(EmployeeRecord other) {
		return id()-other.id();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeRecord other = (EmployeeRecord) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public EmployeeRecord clone() {
		EmployeeRecord res = new EmployeeRecord();
		res.name(name());
		res.document(document());
		res.gender(gender());
		res.age(age());
		res.email(email());
		res.phone(phone());
		res.photo(photo());
		res.roleId(roleId());
		
		return res;
	}

	@Override
	public void id(int nextSequence) {
		this.id = nextSequence;
	}
}
