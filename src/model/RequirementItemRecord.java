package model;

import java.io.Serializable;

import model.RequirementItemRecord;

public class RequirementItemRecord implements Serializable{
	private int roleId;
	private int minimum;
	private int maximum;

	public RequirementItemRecord(int roleId, int minimum, int maximum) {
		roleId(roleId);
		minimum(minimum);
		maximum(maximum);
	}

	public int minimum() {
		return minimum;
	}

	public void minimum(int minimum) {
		this.minimum = minimum;
	}

	public int maximum() {
		return maximum;
	}

	public void maximum(int maximum) {
		this.maximum = maximum;
	}

	public int roleId() {
		return roleId;
	}

	public void roleId(int roleId) {
		this.roleId = roleId;
	}
	
	public boolean validate(int quantity) {
		return (minimum <= quantity && quantity <= maximum());
	}
	
	@Override
	public String toString() {
		return ("rolId = " + roleId + " minimo = " + minimum + ", maximo = " + maximum);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequirementItemRecord other = (RequirementItemRecord) obj;
		if (roleId != other.roleId) 
				return false;
		return true;
	}
	
	public RequirementItemRecord clone() {
		return new RequirementItemRecord(roleId(), minimum(), maximum());
	}
	
}
