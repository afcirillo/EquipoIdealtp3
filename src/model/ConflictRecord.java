package model;

import java.io.Serializable;

public class ConflictRecord implements Comparable<ConflictRecord>, Serializable, Record<ConflictRecord> {
//	public static int sequenceId;
	private int id;
	private int employeeId_1;
	private int employeeId_2;
	
	
	public ConflictRecord(int employeeId_1, int employeeId_2) {
		if (employeeId_1 == employeeId_2)
			throw new IllegalArgumentException("empleado1 no puede ser igual a empleado2");
		employeeId_1(employeeId_1);
		employeeId_2(employeeId_2);
	}

//	@Override
//	public int nextSequence() {
//		id(++sequenceId);
//		return sequenceId;
//	}

	@Override
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public void update(ConflictRecord conflict) {
		employeeId_1(conflict.employeeId_1());
		employeeId_2(conflict.employeeId_2());
	}

	@Override
	public int compareTo(ConflictRecord conflict) {
		return id()-conflict.id();
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + employeeId_1;
		result = prime * result + employeeId_2;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConflictRecord other = (ConflictRecord) obj;
		if (employeeId_1 == other.employeeId_2 && employeeId_2 == other.employeeId_1)
			return true;
		if (employeeId_1 != other.employeeId_1)
			return false;
		if (employeeId_2 != other.employeeId_2)
			return false;
		
		return true;
	}

	public int employeeId_1() {
		return employeeId_1;
	}

	public void employeeId_1(int employeeId_1) {
		this.employeeId_1 = employeeId_1;
	}

	public int employeeId_2() {
		return employeeId_2;
	}

	public void employeeId_2(int employeeId_2) {
		this.employeeId_2 = employeeId_2;
	}

	@Override
	public void id(int nextSequence) {
		this.id = nextSequence;
		
	}
}
