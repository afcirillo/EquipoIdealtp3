package model;

import java.io.Serializable;
import java.util.ArrayList;

public class RequirementRecord implements Serializable, Comparable<RequirementRecord>, Record<RequirementRecord>{
//	public static int sequenceId;
	public enum Status{OPEN, CLOSED};
	private int id;
	private String description;
	private ArrayList<RequirementItemRecord> items;
	private Status status;
	
	public RequirementRecord() {
		items = new ArrayList<RequirementItemRecord>();
		status(Status.OPEN);
	}
	
	public void add(RequirementItemRecord item) {
		if (exists(item))
			return;
		items.add(item);
	}
	
	public boolean exists(RequirementItemRecord item) {
		return items.contains(item);
	}
	
	public int count() {
		return items.size();
	}
	
	public Status status() {
		return status;
	}

	public void status(Status status2) {
		this.status = status2;
	}
	
	public String statusDescription() {
		switch(status){
			case OPEN:
				return "ABIERTO";
			case CLOSED:
				return "CERRADO";
			default:
				return "";			
		}
	}

	public ArrayList<RequirementItemRecord> items() {
		return items;
	} 
	
	public void items(ArrayList<RequirementItemRecord> items) {
		this.items = items;
	}
	
//	protected void id(int id) {
//		this.id = id;
//	}

	public String description() {
		return description;
	}

	public void description(String description) {
		this.description = description;
	}

//	@Override
//	public int nextSequence() {
//		id(++sequenceId);
//		return sequenceId;
//	}

	@Override
	public int id() {
		return id;
	}

	@Override
	public void update(RequirementRecord requirement) {
		description(requirement.description());
		items(requirement.items);
	}

	@Override
	public int compareTo(RequirementRecord other) {
		return id()-other.id();
	}

	@Override
	public String toString() {
		return description();
	}
	
	public RequirementRecord clone() {
		RequirementRecord res = new RequirementRecord();
		res.id(id());
		res.description(description());
		for (RequirementItemRecord item: items)
			res.items().add(new RequirementItemRecord(item.roleId(), item.minimum(),item.maximum()));
		return res;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequirementRecord other = (RequirementRecord) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public void id(int nextSequence) {
		this.id = nextSequence;
		
	}
}
