package interfaces; 

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import controls.Requirement;
import controls.RequirementItem;
import controls.Role;

import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;

public class RequirementForm extends JDialog {

	private JPanel contentPane;
	private JTextField txtDescription;
	private JTextField txtStatus;
	private JButton btnCancel;
	private JButton btnSave;
	
	private ArrayList<Role> roles;
	private Requirement requirement;
	private ArrayList<RequirementItem> items;
	
	private JLabel lblRol;
	private JComboBox<Role> cmbRoles;
	private int state;
	private JLabel lblBanner;
	private JButton btnEdit;
	private JButton btnDelete;
	private JButton btnAdd;
	private JList requirementItemList;
	private JSpinner spnMinimum;
	private JSpinner spnMaximum;
	private JLabel lblDescription;
	
	public RequirementForm(JFrame parent, String title) {
		super(parent, title, true);
		setLocation(100, 100);
		setSize(520, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		items = new ArrayList<RequirementItem>();
	}
	
	public void initComponents() {
		lblDescription = new JLabel("Descripcion *");
		lblDescription.setBounds(9, 81, 77, 14);
		contentPane.add(lblDescription);
		
		txtDescription = new JTextField();
		txtDescription.setBounds(96, 78, 200, 20);
		contentPane.add(txtDescription);
		txtDescription.setColumns(10);
		
		JLabel lblStatus = new JLabel("Estado");
		lblStatus.setBounds(309, 78, 60, 14);
		contentPane.add(lblStatus);
		
		txtStatus = new JTextField();
		txtStatus.setBounds(385, 78, 140, 20);
		txtStatus.setEditable(false);
		contentPane.add(txtDescription);
		
		JLabel lblMinimo = new JLabel("Minimo *");
		lblMinimo.setBounds(9, 150, 60, 14);
		contentPane.add(lblMinimo);
		
		JLabel lblMaximo = new JLabel("Maximo *");
		lblMaximo.setBounds(9, 179, 60, 14);
		contentPane.add(lblMaximo);
		
		btnCancel = new JButton("Cancelar");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				state(JOptionPane.CANCEL_OPTION);
				setVisible(false);
			}
		});
		btnCancel.setBounds(308, 275, 89, 23);
		contentPane.add(btnCancel);
		
		btnSave = new JButton("Guardar");
		btnSave.setBackground(Color.GREEN);
		btnSave.setEnabled(false);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveDataRequirement();
				state(JOptionPane.OK_OPTION);
				setVisible(false);
			}
		});
		btnSave.setBounds(408, 275, 89, 23);
		contentPane.add(btnSave);
		
		lblRol = new JLabel("Rol *");
		lblRol.setBounds(9, 122, 46, 14);
		contentPane.add(lblRol);
		
		cmbRoles = new JComboBox<Role>();
		cmbRoles.setBounds(75, 119, 185, 20);
		cmbRoles.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnAdd.setEnabled(cmbRoles.getSelectedIndex() > 0);	
				btnEdit.setEnabled(false);
				btnDelete.setEnabled(false);
			}
		});
		contentPane.add(cmbRoles);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(9, 262, 488, 2);
		contentPane.add(separator);
		
		lblBanner = new JLabel("REQUERIMIENTO");
		lblBanner.setOpaque(true);
		lblBanner.setBackground(new Color(102, 205, 170));
		lblBanner.setIcon(new ImageIcon("C:\\Users\\A662831\\OneDrive - Atos\\UNGS\\Programacion III\\workspace\\tp3\\images\\64\\requirements.png"));
		lblBanner.setBounds(5, 5, 495, 60);
		contentPane.add(lblBanner);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(270, 120, 227, 120);
		contentPane.add(scrollPane);
		
		requirementItemList = new JList();
		requirementItemList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				loadRequirementItem();
				
				btnAdd.setEnabled(requirementItemList.getSelectedIndex() < 0);
				btnEdit.setEnabled(requirementItemList.getSelectedIndex() >= 0);
				btnDelete.setEnabled(requirementItemList.getSelectedIndex() >= 0);
			}
		});
		scrollPane.setViewportView(requirementItemList);
		
		btnAdd = new JButton(new ImageIcon("images/32/new.png"));
		btnAdd.setBounds(75, 204, 50, 36);
		btnAdd.setEnabled(false);
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addRequirementItem();
				initData();
			}
		});
		contentPane.add(btnAdd);
		
		btnEdit = new JButton(new ImageIcon("images/32/edit.png"));
		btnEdit.setBounds(142, 204, 50, 36);
		btnEdit.setEnabled(false);
		btnEdit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (requirementItemList.getSelectedValue() == null)
					return;
				editRequirementItem();
				initData();
			}
		});
		contentPane.add(btnEdit);
		
		btnDelete = new JButton(new ImageIcon("images/32/delete.png"));
		btnDelete.setBounds(210, 204, 50, 36);
		btnDelete.setEnabled(false);
		btnDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (requirementItemList.getSelectedValue() == null)
					return;
				deleteRequirementItem();
				initData();
			}
		});
		contentPane.add(btnDelete);
		
		spnMinimum = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
		spnMinimum.setBounds(75, 147, 60, 20);
		spnMinimum.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				int min = (Integer)spnMinimum.getValue();
				int max = (Integer)spnMaximum.getValue();
				if(min > max)
					spnMaximum.setValue(min);				
			}
		});
		contentPane.add(spnMinimum);
		
		spnMaximum = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
		spnMaximum.setBounds(75, 176, 60, 20);
		spnMaximum.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				int min = (Integer)spnMinimum.getValue();
				int max = (Integer)spnMaximum.getValue();
				if(min > max)
					spnMinimum.setValue(max);	
			}
		});
		contentPane.add(spnMaximum);
		
		loadCmbRoles();
		loadRequirementData();	
		state(JOptionPane.DEFAULT_OPTION);
	}
	
	protected void loadRequirementItem() {
		// TODO Auto-generated method stub
		if (requirementItemList.getSelectedIndex() < 0)
			return;
		RequirementItem item = (RequirementItem)((ImageItem)requirementItemList.getSelectedValue()).data();
		
		cmbRoles.setSelectedItem(item.role());
		spnMinimum.setValue(item.minimum());
		spnMaximum.setValue(item.maximum());
	}

	protected void deleteRequirementItem() {
		RequirementItem item = (RequirementItem)((ImageItem)requirementItemList.getSelectedValue()).data();
		items.remove(item);
		loadRequirementItemList();
		btnSave.setEnabled(requirementItemList.getModel().getSize() > 0 && !txtDescription.getText().isEmpty());
	}

	protected void editRequirementItem() {
		RequirementItem item = (RequirementItem)((ImageItem)requirementItemList.getSelectedValue()).data();
		try {
			spnMinimum.commitEdit();
			spnMaximum.commitEdit();
		} catch ( java.text.ParseException e ) { 
			throw new RuntimeException("error al parsear");
		}
		item.minimum((Integer)spnMinimum.getValue());
		item.maximum((Integer)spnMaximum.getValue());
		loadRequirementItemList();
	}

	protected void addRequirementItem() {
		Role role = (Role)cmbRoles.getSelectedItem();
		try {
			spnMinimum.commitEdit();
			spnMaximum.commitEdit();
		} catch ( java.text.ParseException e ) { 
			throw new RuntimeException("error al parsear");
		}
		int minimum = (Integer)spnMinimum.getValue();
		int maximum = (Integer)spnMaximum.getValue();
		RequirementItem item = new RequirementItem(role, minimum, maximum);
		if (items.contains(item)) {
			item = items.get(items.indexOf(item));
			item.minimum(minimum);
			item.maximum(maximum);
		}
		else
			items.add(item);
		loadRequirementItemList();
		btnSave.setEnabled(requirementItemList.getModel().getSize() > 0 && !txtDescription.getText().isEmpty());
	}
	
	private void loadRequirementItemList() {
		Role role;
		ArrayList<ImageItem> list = new ArrayList<ImageItem>();
		for (RequirementItem item: items) {
			list.add(new ImageItem(null, item.toString(), item));
		}
		requirementItemList.setListData(list.toArray());
	}

	private void initData() {
		cmbRoles.setSelectedIndex(0);
		spnMinimum.setValue(0);
		spnMaximum.setValue(0);
	}

	public void roles(ArrayList<Role> roles) {
		this.roles = roles;
	}
	
	public void editable(boolean editable) {
		txtDescription.setEditable(editable);
		cmbRoles.setEnabled(editable);
		spnMinimum.setEnabled(editable);
		spnMaximum.setEnabled(editable);
		requirementItemList.setEnabled(editable);
	}
	
	public void requirement(Requirement requirement) {
		this.requirement = requirement;
	}
	
	public Requirement requirement() {
		return requirement;
	}
	
	protected void saveDataRequirement() {	
		if (requirement == null) 
			requirement = new Requirement();

		requirement.description(txtDescription.getText());
		requirement.items(items);
	}

	private void loadCmbRoles() {
		cmbRoles.removeAllItems();
		cmbRoles.addItem(new Role("Seleccionar..."));
		roles.forEach(cmbRoles::addItem);
	}
	
	private void loadRequirementData() {		
		if (requirement == null)
			return;
		txtDescription.setText(requirement.description());
		txtStatus.setText(requirement.status());
		items = requirement.items();
		loadRequirementItemList();
	}
	
	public int state() {
		return state;
	}

	public void state(int state) {
		this.state = state;
	}
}
