package interfaces;

import java.util.ArrayList;
import java.util.stream.Stream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controls.Conflict;
import controls.Employee;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;

public class ConflictForm extends JDialog {

	private JPanel contentPane;
	private JButton btnCancel;
	private JButton btnSave;	
	private Employee employee;
	private ArrayList<Employee> employees;
	private ArrayList<Employee> conflictEmployees;
	private ArrayList<Conflict>	insertedConflicts;
	private ArrayList<Conflict>	deletedConflicts;
	private ArrayList<Conflict> conflicts;
	private int state;
	private JScrollPane scrollPane_1;
	private JComboBox<Employee> cmbEmployees; 
	private JList noConflictList;
	private JList conflictList;
	private JButton btnRight;
	private JButton btnLeft;
	
	public ConflictForm(JFrame parent, String title) {
		super(parent, title, true);
		setLocation(100, 100);
		setSize(530, 385);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);	
		
		JLabel lblBanner = new JLabel("CONFLICTOS");
		lblBanner.setIcon(new ImageIcon("images/64/conflicts.png"));
		lblBanner.setOpaque(true);
		lblBanner.setBackground(new Color(102, 205, 170));
		lblBanner.setBounds(5, 5, 500, 60);
		contentPane.add(lblBanner);
		btnCancel = new JButton("Cancelar");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				state(JOptionPane.CANCEL_OPTION);
				setVisible(false);
			}
		});
		btnCancel.setBounds(309, 310, 89, 23);
		contentPane.add(btnCancel);

		btnSave = new JButton("Guardar");
		btnSave.setBackground(Color.GREEN);
		btnSave.setEnabled(false);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				state(JOptionPane.OK_OPTION);
				setVisible(false);
			}
		});
		btnSave.setBounds(408, 310, 89, 23);
		contentPane.add(btnSave);

		JLabel lblEmployee = new JLabel("Empleado");
		lblEmployee.setBounds(10, 83, 60, 14);
		contentPane.add(lblEmployee);
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 300, 490, 2);
		contentPane.add(separator);

		JLabel lblEmployeesList = new JLabel("Lista de empleados");
		lblEmployeesList.setBounds(10, 105, 120, 14);
		contentPane.add(lblEmployeesList);
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 125, 200, 160);
		contentPane.add(scrollPane_1);

		noConflictList = new JList<Employee>();
		noConflictList.setCellRenderer(new EmployeeRenderer());
		noConflictList.setBorder(new EmptyBorder(5, 5, 5, 5));
		noConflictList.setFixedCellHeight(36);
		noConflictList.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (noConflictList.getSelectedIndex() < 0) {
					btnRight.setEnabled(false);
					return;
				}
				if (e.getClickCount() == 1) {
					btnRight.setEnabled(true);
				}
				if (e.getClickCount() == 2) {
					btnRight.doClick();
				}				
			}			
			@Override public void mouseReleased(MouseEvent arg0) {}			
			@Override public void mousePressed(MouseEvent arg0) {}			
			@Override public void mouseExited(MouseEvent arg0) {}			
			@Override public void mouseEntered(MouseEvent arg0) {}			
		}); 
		
		scrollPane_1.setViewportView(noConflictList);
		
		JLabel lblConflictList = new JLabel("Lista de conflictos");
		lblConflictList.setBounds(300, 105, 120, 14);
		contentPane.add(lblConflictList);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(300, 125, 200, 160);
		contentPane.add(scrollPane);
		
		conflictList = new JList<Employee>();
		conflictList.setCellRenderer(new EmployeeRenderer());
		conflictList.setBorder(new EmptyBorder(5, 5, 5, 5));
		conflictList.setFixedCellHeight(36);
		conflictList.addMouseListener(new MouseListener() {	
			@Override
			public void mouseClicked(MouseEvent e) {
				if (conflictList.getSelectedIndex() < 0) {
					btnLeft.setEnabled(false);
					return;
				}
				if (e.getClickCount() == 1) {
					btnLeft.setEnabled(true);
				}
				if (e.getClickCount() == 2) {
					btnLeft.doClick();
				}				
			}			
			@Override public void mouseReleased(MouseEvent arg0) {}			
			@Override public void mousePressed(MouseEvent arg0) {}			
			@Override public void mouseExited(MouseEvent arg0) {}			
			@Override public void mouseEntered(MouseEvent arg0) {}			
		});
		scrollPane.setViewportView(conflictList);
		
		btnRight = new JButton(">");
		btnRight.setEnabled(false);
		btnRight.setBounds(226, 140, 62, 23);
		btnRight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sendEmployeeToRight();		
				btnRight.setEnabled(false);
			}
		});
		contentPane.add(btnRight);
		btnLeft = new JButton("<");
		btnLeft.setEnabled(false);
		btnLeft.setBounds(226, 220, 62, 23);
		btnLeft.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sendEmployeeToLeft();		
				btnLeft.setEnabled(false);
			}
		});
		contentPane.add(btnLeft);	
		cmbEmployees = new JComboBox<Employee>();
		cmbEmployees.setBounds(100, 80, 180, 20);
		contentPane.add(cmbEmployees);
		
		cmbEmployees.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (cmbEmployees.getSelectedItem() == null)
					return;
				employee((Employee) cmbEmployees.getSelectedItem());
				loadConflictData();
				loadNoConflictData();
				insertedConflicts = new ArrayList<Conflict>();
				deletedConflicts = new ArrayList<Conflict>();
				btnSave.setEnabled(false);
			}
		});
		state(JOptionPane.DEFAULT_OPTION);
		addWindowListener(new WindowListener() {
			@Override 
			public void windowOpened(WindowEvent e) {
				loadEmployees();				
			}			
			@Override public void windowIconified(WindowEvent e) {}			
			@Override public void windowDeiconified(WindowEvent e) {}			
			@Override public void windowDeactivated(WindowEvent e) {}			
			@Override
			public void windowClosing(WindowEvent e) {
				state(JOptionPane.CANCEL_OPTION);				
			}			
			@Override public void windowClosed(WindowEvent e) {}			
			@Override public void windowActivated(WindowEvent e) {}
		});
	}
	
	public void editable(boolean editable) {
		cmbEmployees.setEnabled(editable);
		conflictList.setEnabled(editable);
		noConflictList.setEnabled(editable);
		btnSave.setEnabled(editable);
	}
	
	protected void sendEmployeeToLeft() {
		
		conflictEmployees.remove((Employee) conflictList.getSelectedValue());
		deletedConflicts.add(new Conflict(employee, ((Employee)conflictList.getSelectedValue())));
		insertedConflicts.remove(new Conflict(employee, ((Employee)conflictList.getSelectedValue())));
		conflictList.setListData( conflictEmployees.toArray());
		loadNoConflictData();
		btnSave.setEnabled(true);
	}

	protected void sendEmployeeToRight() {
		conflictEmployees.add((Employee) noConflictList.getSelectedValue());
		Conflict conflict = new Conflict(employee, ((Employee)noConflictList.getSelectedValue()));
		deletedConflicts.remove(conflict);
		insertedConflicts.add(conflict);
		conflictList.setListData(conflictEmployees.toArray());
		loadNoConflictData();
		btnSave.setEnabled(true);
	}
	
	private void loadEmployees() {
		if (employee() != null) {
			cmbEmployees.addItem(employee());
			cmbEmployees.setSelectedItem(employee());
			return;
		}
		Employee noEmployee = new Employee("Seleccionar...", 0, null);
		cmbEmployees.addItem(noEmployee);
		employees.forEach(cmbEmployees::addItem);
		
	}
	
	private void loadNoConflictData() {
		noConflictList.setListData(employees.stream().filter(e -> !conflictEmployees.contains(e) && !employee.equals(e)).toArray());
	}
	
	private void loadConflictData() {
		conflictEmployees = new ArrayList<Employee>();
		for (Conflict c: conflicts()) {
			if (c.employee1().equals(employee()))
				conflictEmployees.add(c.employee2());
			if (c.employee2().equals(employee()))
				conflictEmployees.add(c.employee1());	
		}
		conflictList.setListData(conflictEmployees.toArray());
	}

	public int state() {
		return state;
	}

	public void state(int state) {
		this.state = state;
	}
	public Employee employee() {
		return employee;
	}

	public void employee(Employee employee) {
		this.employee = employee;
	}

	protected ArrayList<Employee> employees() {
		return employees;
	}

	public void employees(ArrayList<Employee> employees) {
		this.employees = employees;
	}

	protected ArrayList<Conflict> conflicts() {
		return conflicts;
	}

	public void conflicts(ArrayList<Conflict> conflicts) {
		this.conflicts = conflicts;
	}

	public ArrayList<Conflict> insertedConflicts() {
		return insertedConflicts;
	}

	public void insertedConflicts(ArrayList<Conflict> insertedConflicts) {
		this.insertedConflicts = insertedConflicts;
	}

	public ArrayList<Conflict> deletedConflicts() {
		return deletedConflicts;
	}

	public void deletedConflicts(ArrayList<Conflict> deletedConflicts) {
		this.deletedConflicts = deletedConflicts;
	}
}
