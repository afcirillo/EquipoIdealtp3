package interfaces; 

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import controls.Employee;
import controls.Picture;
import controls.Role;
import model.Entity;

import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import java.awt.Color;
import java.awt.Component;

public class EmployeeForm extends JDialog {

	private JPanel contentPane;
	private JTextField txtName;
	private JLabel lblPhoto;
	private JComboBox cmbGenders;
	private JFormattedTextField txtDocument;
	private JFormattedTextField txtEmail;
	private JFormattedTextField txtPhone;
	private JButton btnCancel;
	private JButton btnSave;
	
	private ArrayList<Role> roles;
	private Employee employee;
	
	private JLabel lblRol;
	private JComboBox cmbRoles;
	private JLabel lblEdad;
	private JFormattedTextField txtAge;
	private JLabel lblOpenImage;
	private int state;
	private JLabel lblBanner;
	private JLabel lblName;
	private JLabel lblDocument;
	private JLabel lblAge;
	private JLabel lblEmail;
	private JLabel lblPhone;

	
	public EmployeeForm(JFrame parent, String title) {
		super(parent, title, true);
		setLocation(100, 100);
		setSize(524, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblName = new JLabel("Nombre *");
		lblName.setBounds(215, 78, 60, 14);
		contentPane.add(lblName);
		
		txtName = new JTextField();
		txtName.setBounds(275, 75, 220, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		lblDocument = new JLabel("DNI *");
		lblDocument.setBounds(215, 108, 60, 14);
		contentPane.add(lblDocument);
		
		JLabel lblGenero = new JLabel("Genero");
		lblGenero.setBounds(215, 138, 60, 14);
		contentPane.add(lblGenero);
		
		cmbGenders = new JComboBox<String>();
		cmbGenders.setBounds(275, 135, 220, 20);
		
		contentPane.add(cmbGenders);
		
		txtDocument = new JFormattedTextField();
		txtDocument.setBounds(275, 105, 220, 20);
		contentPane.add(txtDocument);
		
		lblEmail = new JLabel("Email");
		lblEmail.setBounds(215, 198, 60, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JFormattedTextField();
		txtEmail.setBounds(275, 195, 220, 20);
		contentPane.add(txtEmail);
		
		lblPhone = new JLabel("Telefono");
		lblPhone.setBounds(215, 228, 60, 14);
		contentPane.add(lblPhone);
		
		txtPhone = new JFormattedTextField();
		txtPhone.setBounds(275, 225, 220, 20);
		contentPane.add(txtPhone);
		
		btnCancel = new JButton("Cancelar");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				state(JOptionPane.CANCEL_OPTION);
				setVisible(false);
			}
		});
		btnCancel.setBounds(309, 307, 89, 23);
		contentPane.add(btnCancel);
		
		btnSave = new JButton("Guardar");
		btnSave.setBackground(Color.GREEN);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!validateData())
					return;
				saveDataEmployee();
				state(JOptionPane.OK_OPTION);
				setVisible(false);
			}
		});
		btnSave.setBounds(408, 307, 89, 23);
		contentPane.add(btnSave);
		
		lblRol = new JLabel("Rol *");
		lblRol.setBounds(215, 255, 46, 14);
		contentPane.add(lblRol);
		
		cmbRoles = new JComboBox<Role>();
		cmbRoles.setBounds(275, 255, 221, 20);
		
		contentPane.add(cmbRoles);
		
		lblAge = new JLabel("Edad");
		lblAge.setBounds(215, 168, 32, 14);
		contentPane.add(lblAge);
		
		txtAge = new JFormattedTextField();
		txtAge.setBounds(276, 165, 221, 20);
		contentPane.add(txtAge);
		
		lblOpenImage = new JLabel("");
		showCamera(true);
		lblOpenImage.setBounds(146, 212, 64, 64);
		lblOpenImage.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String file = OpenImage();	
				if (file == "")
					return;
				lblPhoto.setIcon(getImageWithfixedSize(new ImageIcon(file).getImage(), lblPhoto.getWidth(), lblPhoto.getHeight()));
				showCamera(false);
			}
			@Override public void mouseExited(MouseEvent e) {
				if (employee == null)
					return;
				showCamera(false);
			}			
			@Override public void mouseEntered(MouseEvent e) {
				showCamera(true);
			}	
			@Override public void mouseReleased(MouseEvent e) {}			
			@Override public void mousePressed(MouseEvent e) {}			
					
		});
		contentPane.add(lblOpenImage);
		
		lblPhoto = new JLabel("");
		lblPhoto.setHorizontalAlignment(SwingConstants.CENTER);
		lblPhoto.setBounds(9, 76, 200, 200);
	
		contentPane.add(lblPhoto);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(9, 294, 488, 2);
		contentPane.add(separator);
		
		lblBanner = new JLabel("EMPLEADOS");
		lblBanner.setOpaque(true);
		lblBanner.setBackground(new Color(102, 205, 170));
		lblBanner.setIcon(new ImageIcon("images/64/employees.png"));
		lblBanner.setBounds(5, 5, 500, 60);
		contentPane.add(lblBanner);
		state(JOptionPane.DEFAULT_OPTION);
		addWindowListener(new WindowListener() {
			@Override
			public void windowOpened(WindowEvent e) {
				loadCmbGenders();
				loadCmbRoles();
				loadEmployeeData();				
			}			
			@Override public void windowIconified(WindowEvent e) {}			
			@Override public void windowDeiconified(WindowEvent e) {}			
			@Override public void windowDeactivated(WindowEvent e) {}			
			@Override public void windowClosing(WindowEvent e) {
				state(JOptionPane.CANCEL_OPTION);
			}			
			@Override public void windowClosed(WindowEvent e) {}			
			@Override public void windowActivated(WindowEvent e) {}
		});
	}
	
	public void editable(boolean editable) {
		txtName.setEditable(editable);
		txtDocument.setEditable(editable);
		cmbGenders.setEnabled(editable);
		txtAge.setEditable(editable);
		txtEmail.setEditable(editable);
		txtPhone.setEditable(editable);
		cmbRoles.setEnabled(editable);
		lblOpenImage.setEnabled(editable);
		btnSave.setEnabled(editable);
	}
	
	public void employee(Employee employee) {
		this.employee = employee;
	}
	
	public Employee employee() {
		return employee;
	}
	
	protected void showCamera(boolean show) {
		lblOpenImage.setIcon((show)?new ImageIcon("images/64/camera.png"):null);
	}

	protected String OpenImage() {
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Imagenes (JPG|PNG|BMP)", "jpg", "png", "bmp");
		chooser.setFileFilter(filter);
		chooser.setDialogTitle("Seleccionar imagen");
		chooser.setApproveButtonText("Abrir");
		//chooser.setCurrentDirectory(new File(""));

		if (chooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION)
			return "";
		return chooser.getSelectedFile().getAbsolutePath();	
	}
	
	protected void saveDataEmployee() {		
		if (employee == null)
			employee = new Employee();
		
		employee.name(txtName.getText());
		employee.document(Integer.parseInt((txtDocument.getText().isEmpty())?"0":txtDocument.getText()));
		employee.gender(((String)cmbGenders.getSelectedItem()));
		employee.age(Integer.parseInt((txtAge.getText().isEmpty())?"0":txtAge.getText()));
		employee.email(txtEmail.getText());
		employee.phone(txtPhone.getText());
		employee.role(((Role)cmbRoles.getSelectedItem()));
		employee.photo(imageFromIcon(lblPhoto.getIcon()));	
	}
	
	public void roles(ArrayList<Role> roles) {
		this.roles = roles;
	}

	private void loadCmbRoles() {
		cmbRoles.addItem(new Role("Sin especificar"));
		roles.forEach(cmbRoles::addItem);
	}
	
	private void loadCmbGenders() {
		String[] genders = {"Sin especificar", "Masculino", "Femenino"};
		for (String gender: genders)
			cmbGenders.addItem(gender);
	}
	
	private void loadEmployeeData() {	
		if (employee == null) {
			setDefaultPhoto();
			return;
		}
		txtName.setText(employee.name());
		txtDocument.setText(String.valueOf(employee.document()));
		cmbGenders.setSelectedItem(employee.gender());
		txtAge.setText(String.valueOf(employee.age()));
		txtEmail.setText(employee.email());
		txtPhone.setText(employee.phone());
		cmbRoles.setSelectedItem(employee.role());
		if (employee.photo() == null)
			setDefaultPhoto();
		else
			lblPhoto.setIcon(employee.photo());
	}
	
	private void setDefaultPhoto() {
		lblPhoto.setIcon(getImageWithfixedSize(new ImageIcon("images/32/no_photo.png").getImage(), lblPhoto.getWidth(), lblPhoto.getHeight()));	
	}

	public static ImageIcon getImageWithfixedSize(Image image, int width, int height) {
		if (image == null)
			return null;
		double ratio = ((double)image.getWidth(null))/((double)image.getHeight(null));
		int newWidth = width;
		int newHeight = (int)(width/ratio);
		BufferedImage bi = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.getGraphics();
		g.drawImage(image, 0, 0, null);
		Image newImage = bi.getScaledInstance(newWidth, newHeight, BufferedImage.SCALE_SMOOTH);
		
		return new ImageIcon(newImage);
	}
	
	public static ImageIcon imageFromIcon(Icon icon) {
		if (icon == null)
			return null;
		BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        icon.paintIcon(null, image.getGraphics(), 0, 0);
      
		return new ImageIcon(image);
	}

	public int state() {
		return state;
	}

	public void state(int state) {
		this.state = state;
	}
	
	public boolean validateData() {
		boolean validate = true;
		lblName.setForeground(Color.BLACK);
		lblDocument.setForeground(Color.BLACK);
		lblAge.setForeground(Color.BLACK);
		lblEmail.setForeground(Color.BLACK);
		if (txtName.getText().isEmpty()) {
			lblName.setForeground(Color.RED);
			validate = false;
		}
		if (!txtDocument.getText().matches("\\d\\d\\d\\d\\d\\d\\d\\d")) {
			lblDocument.setForeground(Color.RED);
			validate = false;
		}
		if (!txtAge.getText().isEmpty() && !txtAge.getText().matches("\\d\\d")) {
			lblAge.setForeground(Color.RED);
			validate = false;
		}
		if (!txtEmail.getText().isEmpty() && !txtEmail.getText().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
			lblEmail.setForeground(Color.RED);
			validate = false;
		}
		if (!txtPhone.getText().isEmpty() && !txtPhone.getText().matches("[+]?[\\d]+[-| ]?[\\d]+[-| ]?[\\d]+")) {
			lblPhone.setForeground(Color.RED);
			validate = false;
		}
		
		if (!validate)
			MessageDialog.showError(this, "Revisar los campos resaltados con rojo");
		return validate;
	}
}
