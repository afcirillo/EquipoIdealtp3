package interfaces;

import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import controls.Employee;
import controls.Requirement;
import controls.Role;
import controls.Team;
import model.DataInterface;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JList;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import javax.swing.JLayeredPane;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.Color;

public class MainForm extends JFrame {
	DataInterface rrhh;
	enum Modules {
		EMPLOYEES, ROLES, CONFLICTS, REQUIREMENTS, TEAMS
	};
	private JPanel contentPane;
	private JList genericList;
	private JList modulesList;
	private JButton btnNew;
	private JButton btnEdit;
	private JButton btnDelete;
	private JPanel genericPanel;
	private JPanel panel;
	private JLayeredPane rightLayer;
	private JLabel statusBarCenter;
	private JLabel statusBarLeft;
	private JLabel statusBarRight;
	private JButton btnView;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm frame = new MainForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public MainForm() {
		rrhh = new DataInterface();
		setTitle("RRHH");
		setResizable(true);
		setMinimumSize(new Dimension(800, 600));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		modulesList = new JList<ImageItem>();
		loadModules();
		loadEntities();
		modulesList.setBackground(new Color(102, 205, 170));
		modulesList.setBorder(new EmptyBorder(5, 5, 5, 5));
		modulesList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				Modules module = (Modules) ((ImageItem) modulesList.getSelectedValue()).data();
				btnEdit.setEnabled(false);
				btnDelete.setEnabled(false);
				switch (module) {
				case EMPLOYEES:
					genericList.setCellRenderer(new EmployeeRenderer());
					updateEmployeesList();
					break;
				case ROLES:
					genericList.setCellRenderer(new ItemRender());
					updateRoleList();
					break;
				case CONFLICTS:
					genericList.setCellRenderer(new EmployeeRenderer());
					updateConflictList();
					break;
				case REQUIREMENTS:
					genericList.setCellRenderer(new ItemRender());
					updateRequirementList();
					break;
				case TEAMS:
					genericList.setCellRenderer(new ItemRender());
					updateTeamList();
					break;
				}
			}
		});
		modulesList.setCellRenderer(new ItemRender());
		modulesList.setFixedCellWidth(200);
		contentPane.add(modulesList, BorderLayout.WEST);
		rightLayer = new JLayeredPane();
		contentPane.add(rightLayer, BorderLayout.CENTER);
		rightLayer.setLayout(new BorderLayout(0, 0));
		JToolBar toolBar = new JToolBar();
		toolBar.setBackground(new Color(102, 205, 170));
		rightLayer.add(toolBar, BorderLayout.NORTH);
		toolBar.setFloatable(false);
		btnNew = new JButton("Nuevo");
		btnNew.setBackground(new Color(102, 205, 170));
		btnNew.setBorderPainted(false);
		btnNew.setEnabled(true);
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modules module = (Modules) ((ImageItem) modulesList.getSelectedValue()).data();
				switch (module) {
				case EMPLOYEES:
					newEmployee();
					break;
				case ROLES:
					newRole();
					break;
				case CONFLICTS:
					newConflict();
					break;
				case REQUIREMENTS:
					newRequirement();
					break;
				case TEAMS:
					newTeam();
					break;
				}
			}
		});
		btnNew.setIcon(new ImageIcon("images/32/new.png"));
		toolBar.add(btnNew);
		btnView = new JButton("Ver");
		btnView.setBackground(new Color(102, 205, 170));
		btnView.setBorderPainted(false);
		btnView.setEnabled(false);
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modules module = (Modules) ((ImageItem) modulesList.getSelectedValue()).data();
				switch (module) {
				case EMPLOYEES:
					viewEmployee();
					break;
				case ROLES:
					viewRole();
					break;
				case CONFLICTS:
					viewConflict();
					break;
				case REQUIREMENTS:
					viewRequirement();
					break;
				case TEAMS:
					viewTeam();
					break;
				}
			}
		});
		btnView.setIcon(new ImageIcon("images/32/view.png"));
		toolBar.add(btnView);
		btnEdit = new JButton("Modificar");
		btnEdit.setBackground(new Color(102, 205, 170));
		btnEdit.setBorderPainted(false);
		btnEdit.setEnabled(false);
		btnEdit.setIcon(new ImageIcon("images/32/edit.png"));
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modules module = (Modules) ((ImageItem) modulesList.getSelectedValue()).data();
				switch (module) {
				case EMPLOYEES:
					editEmployee();
					break;
				case ROLES:
					editRole();
					break;
				case CONFLICTS:
					editConflict();
					break;
				case REQUIREMENTS:
					editRequirement();
					break;
				case TEAMS:
					editTeam();
					break;
				}
			}
		});
		toolBar.add(btnEdit);
		btnDelete = new JButton("Eliminar");
		btnDelete.setBackground(new Color(102, 205, 170));
		btnDelete.setBorderPainted(false);
		btnDelete.setEnabled(false);
		btnDelete.setIcon(new ImageIcon("images/32/delete.png"));
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modules module = (Modules) ((ImageItem) modulesList.getSelectedValue()).data();
				switch (module) {
				case EMPLOYEES:
					deleteEmployee();
					break;
				case ROLES:
					deleteRole();
					break;
				case CONFLICTS:
					deleteConflict();
					break;
				case REQUIREMENTS:
					deleteRequirement();
					break;
				case TEAMS:
					deleteTeam();
					break;
				}
			}
		});
		toolBar.add(btnDelete);
		genericPanel = new JPanel();
		rightLayer.add(genericPanel, BorderLayout.CENTER);
		genericPanel.setLayout(new BorderLayout(0, 0));
		genericList = new JList();
		genericList.setCellRenderer(new EmployeeRenderer());
		genericList.setBorder(new EmptyBorder(5, 5, 5, 5));
		genericList.setFixedCellHeight(36);
		
		JScrollPane genericScroll = new JScrollPane(genericList);
		genericPanel.add(genericScroll, BorderLayout.CENTER);
		genericList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		genericList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				btnView.setEnabled(genericList.getSelectedIndex() >= 0);
				btnEdit.setEnabled(genericList.getSelectedIndex() >= 0);
				btnDelete.setEnabled(genericList.getSelectedIndex() >= 0);
			}
		});
		genericList.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (genericList.getSelectedIndex() < 0)
					return;
				if (e.getClickCount() == 2) {
					btnView.doClick();
				}
			}
			@Override public void mouseReleased(MouseEvent arg0) {}			
			@Override public void mousePressed(MouseEvent arg0) {}			
			@Override public void mouseExited(MouseEvent arg0) {}			
			@Override public void mouseEntered(MouseEvent arg0) {}			
		});
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new GridLayout(0, 3, 0, 0));
		statusBarLeft = new JLabel("");
		panel.add(statusBarLeft);
		statusBarCenter = new JLabel("");
		panel.add(statusBarCenter);
		statusBarRight = new JLabel(" ");
		panel.add(statusBarRight);
		updateEmployeesList();
	}
	private void updateEmployeesList() {
		genericList.setListData(rrhh.employees().toArray());
		statusBarLeft.setText(String.valueOf(rrhh.employees().size()) + " elementos");
	}
	private void updateRoleList() {
		ArrayList<ImageItem> roles = new ArrayList<ImageItem>();
		for (Role role: rrhh.roles()) {
			roles.add(new ImageItem(new ImageIcon("images/32/roles.png"), role.toString(), (Object)role));
		}
		genericList.setListData(roles.toArray());
		statusBarLeft.setText(roles.size() + " elementos");
	}
	private void updateConflictList() {
		genericList.setListData(rrhh.getEmployeeWithConflicts().toArray());
		statusBarLeft.setText(rrhh.getEmployeeWithConflicts().size() + " elementos");
	}
	protected void updateRequirementList() {
		ArrayList<ImageItem> requirements = new ArrayList<ImageItem>();
		for (Requirement requirement: rrhh.requirements()) {
			requirements.add(new ImageItem(new ImageIcon("images/32/requirements.png"), requirement.toString(), (Object)requirement));
		}
		genericList.setListData(requirements.toArray());		
		statusBarLeft.setText(requirements.size() + " elementos");
	}
	
	protected void updateTeamList() {
		ArrayList<ImageItem> teams = new ArrayList<ImageItem>();
		for (Team team: rrhh.teams()) {
			teams.add(new ImageItem(new ImageIcon("images/32/team.png"), team.toString(), (Object)team));
		}
		genericList.setListData(teams.toArray());		
		statusBarLeft.setText(teams.size() + " elementos");
	}
	protected void newEmployee() {
		try {
			EmployeeForm employeeForm = new EmployeeForm(this, "Nuevo empleado");
			employeeForm.roles(rrhh.roles());
			employeeForm.setVisible(true);
			if (employeeForm.state() != JOptionPane.OK_OPTION)
				return;
			rrhh.addEmployee(employeeForm.employee());
			updateEmployeesList();
		} catch (Exception e) {
			System.out.println("error new employee");
		}
	}
	protected void viewEmployee() {
		if (genericList.getSelectedIndex() < 0)
			return;
		try {
			EmployeeForm employeeForm = new EmployeeForm(this, "Ver Empleado");
			employeeForm.employee((Employee) genericList.getSelectedValue());
			employeeForm.roles(rrhh.roles());
			employeeForm.editable(false);
			employeeForm.setVisible(true);
			if (employeeForm.state() != JOptionPane.OK_OPTION)
				return;
			rrhh.editEmployee(employeeForm.employee());
			updateEmployeesList();
		} catch (Exception e) {
			System.out.println("error edit employee");
		}	
	}
	protected void editEmployee() {
		if (genericList.getSelectedIndex() < 0)
			return;
		try {
			EmployeeForm employeeForm = new EmployeeForm(this, "Modificar empleado");
			employeeForm.employee((Employee) genericList.getSelectedValue());
			employeeForm.roles(rrhh.roles());
			employeeForm.setVisible(true);
			if (employeeForm.state() != JOptionPane.OK_OPTION)
				return;
			rrhh.editEmployee(employeeForm.employee());
			updateEmployeesList();
		} catch (Exception e) {
			System.out.println("error edit employee");
		}
	}
	protected void deleteEmployee() {
		if (genericList.getSelectedIndex() < 0)
			return;
		Employee selected = (Employee) genericList.getSelectedValue();
		rrhh.deleteEmployee(selected);
		updateEmployeesList();
	}
	// ROLE --------------------------
	protected void newRole() {
		try {
			RoleForm roleForm = new RoleForm(this, "Nuevo rol", null);
			boolean exists;
			do {
				roleForm.setVisible(true);
				if (roleForm.state() != JOptionPane.OK_OPTION)
					return;
				exists = (rrhh.roles().stream().filter(r -> r.description().equals(roleForm.role().description())).count() > 0);
				if (exists)
					MessageDialog.showError(this, "No se puede repetir el nombre del rol");
			}while(exists);
			rrhh.addRole(roleForm.role());
			updateRoleList();
		} catch (Exception e) {
			System.out.println("error new role");
		}
	}
	protected void viewRole() {
		if (genericList.getSelectedIndex() < 0)
			return;
		try {
			RoleForm roleForm = new RoleForm(this, "Ver Rol", (Role) ((ImageItem)genericList.getSelectedValue()).data());
			roleForm.editable(false);
			roleForm.setVisible(true);
			if (roleForm.state() != JOptionPane.OK_OPTION)
				return;
			rrhh.editRole(roleForm.role());
			updateRoleList();
		} catch (Exception e) {
			System.out.println("error edit role");
		}
	}
	protected void editRole() {
		if (genericList.getSelectedIndex() < 0)
			return;
		try {
			RoleForm roleForm = new RoleForm(this, "Modificar rol", (Role) ((ImageItem)genericList.getSelectedValue()).data());
			roleForm.role((Role) ((ImageItem)genericList.getSelectedValue()).data());
			roleForm.setVisible(true);
			if (roleForm.state() != JOptionPane.OK_OPTION)
				return;
			rrhh.editRole(roleForm.role());
			updateRoleList();
		} catch (Exception e) {
			System.out.println("error edit role");
		}
	}
	protected void deleteRole() {
		if (genericList.getSelectedIndex() < 0)
			return;
		Role selected = (Role)((ImageItem)genericList.getSelectedValue()).data();
		rrhh.deleteRole(selected);
		updateRoleList();
	}
	protected void newConflict() {
		try {
			ConflictForm conflictForm = new ConflictForm(this, "Nuevo Conflicto");
			conflictForm.employees(rrhh.employees());
			conflictForm.conflicts(rrhh.conflicts());
			conflictForm.setVisible(true);
			if (conflictForm.state() != JOptionPane.OK_OPTION)
				return;
			conflictForm.deletedConflicts().forEach(rrhh::deleteConflict);
			conflictForm.insertedConflicts().forEach(rrhh::addConflict);
			updateConflictList();
		} catch (Exception e) {
			System.out.println("error new conflict");
		}
	}
	protected void viewConflict() {
		try {
			ConflictForm conflictForm = new ConflictForm(this, "Ver Conflicto");
			conflictForm.employee((Employee)genericList.getSelectedValue());
			conflictForm.employees(rrhh.employees());
			conflictForm.conflicts(rrhh.conflicts());
			conflictForm.editable(false);
			conflictForm.setVisible(true);
		} catch (Exception e) {
			System.out.println("error edit conflict");
		}	
	}
	protected void editConflict() {
		try {
			ConflictForm conflictForm = new ConflictForm(this, "Modificar Conflicto");
			conflictForm.employee((Employee)genericList.getSelectedValue());
			conflictForm.employees(rrhh.employees());
			conflictForm.conflicts(rrhh.conflicts());
			conflictForm.setVisible(true);
			if (conflictForm.state() != JOptionPane.OK_OPTION)
				return;
			conflictForm.deletedConflicts().forEach(rrhh::deleteConflict);
			conflictForm.insertedConflicts().forEach(rrhh::addConflict);
			updateConflictList();
		} catch (Exception e) {
			System.out.println("error edit conflict");
		}
	}
	protected void deleteConflict() {
		rrhh.deleteConflictFrom((Employee)genericList.getSelectedValue());
		updateConflictList();
	}
	protected void newRequirement() {
		try {
			boolean exists;
			RequirementForm requirementForm = new RequirementForm(this, "Nuevo Requerimiento");
			requirementForm.roles(rrhh.roles());
			requirementForm.initComponents();			
			do {
				requirementForm.setVisible(true);
				if (requirementForm.state() != JOptionPane.OK_OPTION)
					return;
				exists = (rrhh.requirements().stream().filter(r -> r.description().equals(requirementForm.requirement().description())).count() > 0);
				if (exists)
					MessageDialog.showError(this, "No se puede repetir la descripción");
			}while(exists);
			rrhh.addRequirement(requirementForm.requirement());
			updateRequirementList();
		} catch (Exception e) {
			System.out.println("error new requirement");
		}
	}
	protected void viewRequirement() {
		try {
			RequirementForm requirementForm = new RequirementForm(this, "Ver Requerimiento");
			requirementForm.requirement((Requirement)((ImageItem)genericList.getSelectedValue()).data());
			requirementForm.roles(rrhh.roles());
			requirementForm.initComponents();
			requirementForm.editable(false);
			requirementForm.setVisible(true);
		} catch (Exception e) {
			System.out.println("error view rquirement");
		}	
	}
	protected void editRequirement() {
		try {
			RequirementForm requirementForm = new RequirementForm(this, "Modificar Requerimiento");
			requirementForm.requirement(((Requirement)((ImageItem)genericList.getSelectedValue()).data()).clone());
			requirementForm.roles(rrhh.roles());
			requirementForm.initComponents();
			requirementForm.setVisible(true);
			if (requirementForm.state() != JOptionPane.OK_OPTION)
				return;
			rrhh.editRequirement(requirementForm.requirement());
			updateRequirementList();
		} catch (Exception e) {
			System.out.println("error edit requirement");
		}
	}
	protected void deleteRequirement() {
		rrhh.deleteRequirement((Requirement)((ImageItem)genericList.getSelectedValue()).data());
		updateRequirementList();
	}
	
	protected void newTeam() {
		try {
			TeamForm teamForm = new TeamForm(this, "Nuevo Requerimiento");
			teamForm.requirements(rrhh.requirements());
			teamForm.employees(rrhh.employees());
			teamForm.conflicts(rrhh.conflicts());
			teamForm.setVisible(true);
			if (teamForm.state() != JOptionPane.OK_OPTION)
				return;
			rrhh.addTeam(teamForm.team());
			updateTeamList();
		} catch (Exception e) {
			System.out.println("error new team: " + e);
		}
	}
	protected void viewTeam() {
		try {
			TeamForm teamForm = new TeamForm(this, "Ver Requerimiento");
			teamForm.requirements(rrhh.requirements());
			teamForm.employees(rrhh.employees());
			teamForm.conflicts(rrhh.conflicts());
			teamForm.team((Team)((ImageItem)genericList.getSelectedValue()).data());
			teamForm.editable(false);
			teamForm.setVisible(true);
		} catch (Exception e) {
			System.out.println("error view team");
		}	
	}
	protected void editTeam() {
		try {
			TeamForm teamForm = new TeamForm(this, "Nuevo Requerimiento");
			teamForm.requirements(rrhh.requirements());
			teamForm.employees(rrhh.employees());
			teamForm.conflicts(rrhh.conflicts());
			teamForm.team((Team)((ImageItem)genericList.getSelectedValue()).data());
			teamForm.setVisible(true);
			
			if (teamForm.state() != JOptionPane.OK_OPTION)
				return;
			rrhh.editTeam(teamForm.team());
			updateTeamList();
		} catch (Exception e) {
			System.out.println("error edit team");
		}
	}
	protected void deleteTeam() {
		rrhh.deleteTeam((Team)((ImageItem)genericList.getSelectedValue()).data());
		updateTeamList();
	}
	private void loadEntities() {
		rrhh.loadEntity();
	}
	private void loadModules() {
		ArrayList<ImageItem> modules = new ArrayList<ImageItem>();
		modules.add(new ImageItem(new ImageIcon("images/64/employees.png"), "Empleados", (Object) Modules.EMPLOYEES));
		modules.add(new ImageItem(new ImageIcon("images/64/roles.png"), "Roles", (Object) Modules.ROLES));
		modules.add(new ImageItem(new ImageIcon("images/64/conflicts.png"), "Conflictos", (Object) Modules.CONFLICTS));
		modules.add(new ImageItem(new ImageIcon("images/64/requirements.png"), "Requerimientos", (Object) Modules.REQUIREMENTS));
		modules.add(new ImageItem(new ImageIcon("images/64/team.png"), "Equipo", (Object) Modules.TEAMS));
		modulesList.setListData(modules.toArray());
		modulesList.setSelectedIndex(0);
	}
}