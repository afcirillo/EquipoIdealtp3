package interfaces;

import javax.swing.ImageIcon;

public class ImageItem implements ItemList{

	private String text;
	private ImageIcon image;
	private Object data;
	
	public ImageItem(ImageIcon image, String text, Object data) {
		this.image = image;
		this.text = text;
		this.data = data;
	}
	@Override
	public String text() {
		// TODO Auto-generated method stub
		return text;
	}
	
	@Override
	public ImageIcon image() {
		// TODO Auto-generated method stub
		return image;
	}
	
	@Override
	public String toString() {
		return text;
	}
	@Override
	public Object data() {
		// TODO Auto-generated method stub
		return data;
	}
	@Override
	public void text(String text) {
		this.text = text;
		
	}
	@Override
	public void image(ImageIcon image) {
		this.image = image;
	}
	@Override
	public void data(Object data) {
		this.data = data;
	}

}
