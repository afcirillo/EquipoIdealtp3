package interfaces;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MessageDialog {
	public static void showError(JDialog parent, String message) {
		JOptionPane.showMessageDialog(parent, message);
	}
	public static void showError(JFrame parent, String message) {
		JOptionPane.showMessageDialog(parent, message);
	}
}
