package interfaces;

import java.util.ArrayList;

import controls.Conflict;
import controls.Employee;
import controls.Requirement;
import controls.RequirementItem;
import controls.Role;
import controls.TeamGenerator;

public class mainPrueba {

	static ArrayList<Employee> empleados;
	static Requirement requisitos;
	static ArrayList<Conflict> incompatibilidades;
	static ArrayList<Role> roles;

	public static void main(String[] args) {
		empleados = new ArrayList<Employee>();
		roles = new ArrayList<Role>();
		incompatibilidades = new ArrayList<Conflict>();

		TeamGenerator team;
		TeamGenerator team2;
		ArrayList<Role> roles = new ArrayList<Role>();
		roles.add(new Role(1, "Lider de proyecto"));
		roles.add(new Role(2, "Arquitecto"));
		roles.add(new Role(3, "Programador"));
		roles.add(new Role(4, "Tester"));

		empleados.add(new Employee(1, "Lider de proyecto 1", 12345678, roles.get(0)));
		empleados.add(new Employee(2, "Lider de proyecto 2", 12345679, roles.get(0)));
		empleados.add(new Employee(3, "Lider de proyecto 3", 12345680, roles.get(0)));
		empleados.add(new Employee(4, "Lider de proyecto 4", 12345681, roles.get(0)));

		empleados.add(new Employee(5, "Arquitecto 1", 12345682, roles.get(1)));
		empleados.add(new Employee(6, "Arquitecto 2", 12345683, roles.get(1)));
		empleados.add(new Employee(7, "Arquitecto 3", 12345684, roles.get(1)));
		empleados.add(new Employee(8, "Arquitecto 4", 12345685, roles.get(1)));
		empleados.add(new Employee(9, "Arquitecto 5", 12345686, roles.get(1)));

		empleados.add(new Employee(10, "Programador 1", 12345687, roles.get(2)));
		empleados.add(new Employee(11, "Programador 2", 12345688, roles.get(2)));
		empleados.add(new Employee(12, "Programador 3", 12345689, roles.get(2)));
		empleados.add(new Employee(13, "Programador 4", 12345690, roles.get(2)));
		empleados.add(new Employee(14, "Programador 5", 12345691, roles.get(2)));
		empleados.add(new Employee(15, "Programador 6", 12345692, roles.get(2)));

		empleados.add(new Employee(16, "Tester 1", 12345693, roles.get(3)));
		empleados.add(new Employee(17, "Tester 2", 12345694, roles.get(3)));
		empleados.add(new Employee(18, "Tester 3", 12345695, roles.get(3)));
		empleados.add(new Employee(19, "Tester 4", 12345696, roles.get(3)));
		empleados.add(new Employee(20, "Tester 5", 12345697, roles.get(3)));
		empleados.add(new Employee(21, "Tester 6", 12345698, roles.get(3)));
		empleados.add(new Employee(22, "Tester 7", 12345699, roles.get(3)));

		requisitos = new Requirement();
		requisitos.add(new RequirementItem(roles.get(0), 1, 2));
		requisitos.add(new RequirementItem(roles.get(1), 2, 3));
		requisitos.add(new RequirementItem(roles.get(2), 3, 6));
		requisitos.add(new RequirementItem(roles.get(3), 4, 5));

		incompatibilidades.add(new Conflict(empleados.get(8), empleados.get(19)));

		System.out.println("EMPLEADOS--------------------");
		for (Employee empleado : empleados) {
			System.out.println(empleado);
		}
		System.out.println("-----------------------------");

		System.out.println("\n\nREQUISITOS-------------------");
		for (RequirementItem item : requisitos.items()) {
			System.out.println(item);
		}
		System.out.println("-----------------------------");

		team = new TeamGenerator(requisitos, empleados, incompatibilidades) {
			@Override
			public void run() {
				resolveByBruteForce();
			}
		};

		team2 = new TeamGenerator(requisitos, empleados, incompatibilidades) {
			@Override
			public void run() {
				resolveByBacktracking2();
			}
		};
		team.start();
		team2.start();

		boolean firstTime = true;
		while (team.isAlive() || team2.isAlive()) {
			if (!team2.isAlive() && firstTime) {
				firstTime = false;
				System.out.println("\n\nRESULTADO BACKTRACKING-------------------------");
				for (Employee employee : team2.result()) {
					System.out.println(employee);
				}
				System.out.println("\nTIEMPO DE EJECUCION: " + team2.elapsedTimeInMs() + " ms");
				System.out.println("-------------------------------------------------");
			}
		}

		// FIN THREAD

		System.out.println("\n\nRESULTADO FUERZA BRUTA-------------------------");
		for (Employee employee : team.result()) {
			System.out.println(employee);
		}

		System.out.println("\nTIEMPO DE EJECUCION: " + team.elapsedTimeInMs() + " ms");
		System.out.println("COMBINACIONES CORRECTAS: " + team.correctCombinations());

		System.out.println("-------------------------------------------------");
	}

}
