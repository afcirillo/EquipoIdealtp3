package interfaces;

import java.awt.Component;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import controls.Employee;
import controls.Picture;

public class EmployeeRenderer extends JLabel implements ListCellRenderer<Employee>{
	
	public EmployeeRenderer() {
		setOpaque(true);
	}
	@Override
	public Component getListCellRendererComponent(JList<? extends Employee> list, Employee employee, int index, boolean isSelected,
			boolean cellHasFocus) {
		
		ImageIcon icon = getImage(employee.photo(), 32, 32);
		setText(employee.toString());
		
		if (icon != null)
			setIcon(icon);
		
		 if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
		return this;
	}
	
	private static ImageIcon getImage(ImageIcon image, int width, int height) {
		if (image == null)
			return null;
		BufferedImage bi = new BufferedImage(image.getIconWidth(), image.getIconHeight(), BufferedImage.TYPE_INT_RGB);
		image.paintIcon(null, bi.getGraphics(), 0, 0);
		Image newImage = bi.getScaledInstance(32, 32, BufferedImage.SCALE_SMOOTH);
		return new ImageIcon(newImage);
	}

}
