package interfaces;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controls.Role;

import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JDialog;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.ImageIcon;

public class RoleForm extends JDialog {

	private JPanel contentPane;
	private JTextField txtRole;
	private JButton btnCancel;
	private JButton btnSave;

	Role role;
	
	private JLabel lblRole;
	private int state;
	private JLabel lblBanner;

	public RoleForm(JFrame parent, String title, Role role) {
		super(parent, title, true);
		setLocation(100, 100);
		setSize(340, 200);
		
		role(role);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtRole = new JTextField();
		txtRole.setBounds(55, 78, 255, 20);
		loadDataRole();
		txtRole.addKeyListener(new KeyListener() {
			@Override public void keyTyped(KeyEvent e) {}			
			@Override public void keyReleased(KeyEvent e) {
				btnSave.setEnabled(!txtRole.getText().isEmpty());
			}			
			@Override public void keyPressed(KeyEvent e) {}
		});
		
		contentPane.add(txtRole);
		txtRole.setColumns(10);

		btnCancel = new JButton("Cancelar");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				state(JOptionPane.CANCEL_OPTION);
				setVisible(false);
			}
		});
		btnCancel.setBounds(122, 122, 89, 23);
		contentPane.add(btnCancel);

		btnSave = new JButton("Guardar");
		btnSave.setBackground(Color.GREEN);
		btnSave.setEnabled(false);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveDataRole();
				state(JOptionPane.OK_OPTION);
				setVisible(false);
			}
		});
		btnSave.setBounds(221, 122, 89, 23);
		contentPane.add(btnSave);

		lblRole = new JLabel("Rol *");
		lblRole.setBounds(10, 81, 46, 14);
		contentPane.add(lblRole);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 109, 301, 2);
		contentPane.add(separator);
		
		lblBanner = new JLabel("ROLES");
		lblBanner.setIcon(new ImageIcon("images/64/roles.png"));
		lblBanner.setOpaque(true);
		lblBanner.setBackground(new Color(102, 205, 170));
		lblBanner.setBounds(5, 5, 310, 60);
		contentPane.add(lblBanner);
		state(JOptionPane.DEFAULT_OPTION);
		addWindowListener(new WindowListener() {
			@Override public void windowOpened(WindowEvent e) {}			
			@Override public void windowIconified(WindowEvent e) {}			
			@Override public void windowDeiconified(WindowEvent e) {}			
			@Override public void windowDeactivated(WindowEvent e) {}			
			@Override public void windowClosing(WindowEvent e) {
				state(JOptionPane.CANCEL_OPTION);				
			}			
			@Override public void windowClosed(WindowEvent e) {}			
			@Override public void windowActivated(WindowEvent e) {}
		});
	}

	public void editable(boolean editable) {
		txtRole.setEditable(editable);
		btnSave.setEnabled(editable);
	}
	
	protected void saveDataRole() {	
		if (role == null)
			role = new Role(txtRole.getText());
		role.description(txtRole.getText());
	}

	private void loadDataRole() {
		if (role == null)
			return;
		txtRole.setText(role.description());
	}

	public int state() {
		return state;
	}

	public void state(int state) {
		this.state = state;
	}

	public Role role() {
		return role;
	}

	public void role(Role role) {
		this.role = role;
	}
}
