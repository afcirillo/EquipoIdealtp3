package interfaces;

import javax.swing.ImageIcon;

public interface ItemList {
	public String text();
	public ImageIcon image();
	public Object data();
	
	public void text(String text);
	public void image(ImageIcon image);
	public void data(Object data);
}
