package interfaces;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controls.Conflict;
import controls.Employee;
import controls.Requirement;
import controls.Team;
import controls.TeamGenerator;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JScrollPane;

public class TeamForm extends JDialog {

	private String[] algorithms = { "Backtracking", "Fuerza bruta" };

	private final JPanel contentPanel = new JPanel();

	private TeamGenerator teamGenerator;
	private Team team;

	private Requirement requirement;

	private ArrayList<Requirement> requirements;
	private ArrayList<Employee> employees;
	private ArrayList<Conflict> conflicts;

	private JComboBox cmbRequirements;
	private JComboBox cmbAlgorithm = new JComboBox();
	private JButton btnGenerateTeam;
	private JList lstEmployees;
	private int state;
	private JLabel lblBanner;
	private JProgressBar progressBar;
	private JButton btnSave;
	private JButton btnCancel;
	private JLabel lblTime;
	private JLabel lblCorrectCombinations;

	Timer timer;
	private JLabel lblItereaciones;
	
	public TeamForm(JFrame parent, String title) {
		super(parent, title, true);
		setResizable(false);
		setBounds(100, 100, 388, 539);
		getContentPane().setLayout(null);

		lblBanner = new JLabel("Equipo");
		lblBanner.setBackground(new Color(102, 205, 170));
		lblBanner.setIcon(new ImageIcon("images/64/team.png"));
		lblBanner.setBounds(10, 11, 363, 66);
		lblBanner.setOpaque(true);
		getContentPane().add(lblBanner);

		JLabel lblRequirement = new JLabel("Requerimiento");
		lblRequirement.setBounds(10, 103, 91, 14);
		getContentPane().add(lblRequirement);

		JLabel lblMethod = new JLabel("Algoritmo");
		lblMethod.setBounds(10, 131, 91, 14);
		getContentPane().add(lblMethod);

		progressBar = new JProgressBar();
		progressBar.setForeground(new Color(0, 204, 0));
		progressBar.setStringPainted(true);
		progressBar.setBounds(10, 198, 363, 14);
		progressBar.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if (progressBar.getValue() != progressBar.getMaximum())
					return;
				timer.stop();
				lblTime.setText("Tiempo: " + teamGenerator.elapsedTimeInMs()/1000 + " s");
				lblCorrectCombinations.setText("Combinaciones correctas: " + teamGenerator.correctCombinations());
				lblItereaciones.setText("Iteraciones: " + teamGenerator.iterations());
				btnSave.setEnabled(teamGenerator.result().length > 0 );
				if (teamGenerator.result().length == 0) {
					MessageDialog.showError((JDialog)null, "No se encontro un equipo que cumpla los requisitos");
				}
				lstEmployees.setListData(teamGenerator.result());
			}
		});
		getContentPane().add(progressBar);
		
		btnSave = new JButton("Guardar");
		btnSave.setBackground(Color.GREEN);
		btnSave.setBounds(282, 476, 89, 23);
		btnSave.setEnabled(false);
		btnSave.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				saveTeam();	
				state(JOptionPane.OK_OPTION);
				setVisible(false);
			}
		});
		getContentPane().add(btnSave);
		
		btnCancel = new JButton("Cancelar");
		btnCancel.setBounds(183, 476, 89, 23);
		btnCancel.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				state(JOptionPane.CANCEL_OPTION);
				setVisible(false);
			}
		});
		getContentPane().add(btnCancel);
		
	
		JSeparator separator = new JSeparator();
		separator.setBounds(11, 408, 362, 2);
		getContentPane().add(separator);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 223, 362, 174);
		getContentPane().add(scrollPane);
		
		lstEmployees = new JList();
		lstEmployees.setCellRenderer(new EmployeeRenderer());
		lstEmployees.setEnabled(false);
		lstEmployees.setBorder(new EmptyBorder(5, 5, 5, 5));
		lstEmployees.setFixedCellHeight(36);
		scrollPane.setViewportView(lstEmployees);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		cmbRequirements = new JComboBox<Requirement>();
		cmbRequirements.setBounds(111, 100, 262, 20);
		cmbRequirements.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				lstEmployees.setListData(new Object[0]);
				btnSave.setEnabled(false);
			}
		});
		getContentPane().add(cmbRequirements);

		cmbAlgorithm = new JComboBox();
		cmbAlgorithm.setBounds(111, 128, 262, 20);
		cmbAlgorithm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lstEmployees.setListData(new Object[0]);
				btnSave.setEnabled(false);
			}
		});
		getContentPane().add(cmbAlgorithm);

		btnGenerateTeam = new JButton("Generar equipo");
		btnGenerateTeam.setBounds(111, 160, 169, 23);
		getContentPane().add(btnGenerateTeam);
		
		lblTime = new JLabel("Tiempo: 0 s");
		lblTime.setBounds(10, 458, 78, 14);
		getContentPane().add(lblTime);
		
		lblCorrectCombinations = new JLabel("Combinaciones correctas: 0");
		lblCorrectCombinations.setBounds(10, 421, 361, 14);
		getContentPane().add(lblCorrectCombinations);
		
		lblItereaciones = new JLabel("Iteraciones: 0");
		lblItereaciones.setBounds(10, 439, 224, 14);
		getContentPane().add(lblItereaciones);

		btnGenerateTeam.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
		
				lstEmployees.setListData(new Object[0]);
				requirement = (Requirement) cmbRequirements.getSelectedItem();

				lblTime.setText("Tiempo: 0" + " s");
				lblCorrectCombinations.setText("Combinaciones correctas: 0");
				lblItereaciones.setText("Iteraciones: 0");
				switch (cmbAlgorithm.getSelectedItem().toString()) {
				case "Backtracking":
					teamGenerator = new TeamGenerator(requirement, employees, conflicts) {
						public void run() {
							resolveByBacktracking2();
						}
					};
					teamGenerator.start();
					timer.start();
					break;

				case "Fuerza bruta":
					teamGenerator = new TeamGenerator(requirement, employees, conflicts) {
						public void run() {
							resolveByBruteForce();
						}
					};
					teamGenerator.start();
					timer.start();
					break;
				}
			}
		});
		
		timer = new Timer(50, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateProgressBar();
			}
		});

		addWindowListener(new WindowListener() {
			@Override
			public void windowOpened(WindowEvent e) {
				loadCmbRequirements();
				loadCmbAlgorthms();
				loadTeam();				
			}			
			@Override public void windowIconified(WindowEvent e) {}			
			@Override public void windowDeiconified(WindowEvent e) {}			
			@Override public void windowDeactivated(WindowEvent e) {}			
			@Override public void windowClosing(WindowEvent e) {}			
			@Override 
			public void windowClosed(WindowEvent e) {
				state(JOptionPane.CANCEL_OPTION);
			}			
			@Override public void windowActivated(WindowEvent e) {}
		});
		state(JOptionPane.DEFAULT_OPTION);
	}

	protected void loadCmbRequirements() {
		requirements.forEach(r -> cmbRequirements.addItem(r));
	}
	
	protected void loadCmbAlgorthms() {
		for (String alg : algorithms) {
			cmbAlgorithm.addItem(alg);
		}
	}
	
	protected void saveTeam() {
		if (team == null)
			team = new Team();
		team.requirement((Requirement) cmbRequirements.getSelectedItem());
		team.algorithm((String)cmbAlgorithm.getSelectedItem());
		team.correctCombinations(teamGenerator.correctCombinations());
		team.iterations(teamGenerator.iterations());
		team.elapsedTimeInMs(teamGenerator.elapsedTimeInMs());
		team.employees(new ArrayList<Employee>());
		for (Employee employee: teamGenerator.result()) {
			team.add(employee);
		}
	}
	
	private void loadTeam() {
		if (team == null)
			return;
		cmbRequirements.setSelectedItem(team.requirement());
		cmbAlgorithm.setSelectedItem(team.algorithm());
		lblCorrectCombinations.setText("Combinaciones correctas: " + team.correctCombinations());
		lblItereaciones.setText("Iteraciones: " + team.iterations());
		lblTime.setText("Tiempo: " + (team.elapsedTimeInMs()/1000) + " s");
		lstEmployees.setListData(team.employees().toArray());
	}
	
	private void updateProgressBar() {
		progressBar.setValue((int)teamGenerator.taskProgress());
	}

	public int state() {
		return state;
	}

	public void state(int state) {
		this.state = state;
	}

	public void requirement(Requirement requirement) {
		this.requirement = requirement;
	}

	public void requirements(ArrayList<Requirement> requirements) {
		this.requirements = requirements;
	}

	public void employees(ArrayList<Employee> employees) {
		this.employees = employees;
	}
	
	public void team(Team team) {
		this.team = team.clone();
	}

	public void conflicts(ArrayList<Conflict> conflicts) {
		this.conflicts = conflicts;
	}

	public Team team() {
		return team;
	}

	public void editable(boolean editable) {
		cmbRequirements.setEnabled(editable);
		cmbAlgorithm.setEnabled(editable);
		btnGenerateTeam.setEnabled(editable);
		btnSave.setEnabled(editable);
	}
}
