package interfaces;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import controls.Picture;

public class ItemRender<T extends ItemList> extends JLabel implements ListCellRenderer<T>{
	
	
	public ItemRender() {
		setOpaque(true);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends T> list, T value, int index, boolean isSelected,
			boolean cellHasFocus) {
		ImageIcon icon = value.image();
		setText(value.text());
		if (icon != null)
			setIcon(icon);
		
		 if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
		return this;
	}
	
}
