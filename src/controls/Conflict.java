package controls;

public class Conflict{
	private Employee employee1;
	private Employee employee2;
		
	public Conflict(Employee employeeId_1, Employee employeeId_2) {
		if (employeeId_1.equals(employeeId_2))
			throw new IllegalArgumentException("empleado1 no puede ser igual a empleado2");
		employee1(employeeId_1);
		employee2(employeeId_2);
	}	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conflict other = (Conflict) obj;
		if (employee1.id() == other.employee2.id() && employee2.id() == other.employee1.id())
			return true;
		if (employee1.id() != other.employee1.id())
			return false;
		if (employee2.id() != other.employee2.id())
			return false;
		
		return true;
	}

	public Employee employee1() {
		return employee1;
	}

	public void employee1(Employee employee1) {
		this.employee1 = employee1;
	}

	public Employee employee2() {
		return employee2;
	}

	public void employee2(Employee employee2) {
		this.employee2 = employee2;
	}
}
