package controls;

import java.util.ArrayList;

public class Team {
	private int id;
	private Requirement requirement;
	private String algorithm;
	private int elapsedTimeInMs;
	private int correctCombinations;
	private int iterations;
	private ArrayList<Employee> employees;
	
	public Team() {
		employees = new ArrayList<Employee>();
	}

	public void  add(Employee employee) {
		if (exists(employee))
			return;
		employees.add(employee);
	}
	
	private boolean exists(Employee employee) {
		return employees.contains(employee);
	}
	
	public void remove(Employee employee) {
		employees.remove(employee);
	}

	public int id() {
		return id;
	}

	public void id(int id) {
		this.id = id;
	}

	public Requirement requirement() {
		return requirement;
	}

	public void requirement(Requirement requirement) {
		this.requirement = requirement;
	}

	public String algorithm() {
		return algorithm;
	}

	public void algorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public ArrayList<Employee> employees() {
		return employees;
	}

	public void employees(ArrayList<Employee> employees) {
		this.employees = employees;
	}
	
	@Override 
	public String toString() {
		return requirement.description() + "(" + algorithm() + ")";
	}
	
	public Team clone() {
		Team res = new Team();
		res.id(id());
		res.requirement(requirement.clone());
		res.algorithm(algorithm);
		res.correctCombinations(correctCombinations);
		res.iterations(iterations);
		res.elapsedTimeInMs(elapsedTimeInMs);
		for (Employee employee : employees)
			res.employees.add(employee.clone());
		return res;
	}

	public int elapsedTimeInMs() {
		return elapsedTimeInMs;
	}

	public void elapsedTimeInMs(int elapsedTimeInMs) {
		this.elapsedTimeInMs = elapsedTimeInMs;
	}

	public int correctCombinations() {
		return correctCombinations;
	}

	public void correctCombinations(int correctCombinations) {
		this.correctCombinations = correctCombinations;
	}

	public int iterations() {
		return iterations;
	}

	public void iterations(int iterations) {
		this.iterations = iterations;
	}

}
