package controls;

import javax.swing.ImageIcon;

public class Employee {
	private int id;
	private int document;
	private String name;
	private String gender;
	private int age;
	private String email;
	private String phone;
	private ImageIcon photo;
	private Role role;

	public Employee() {
	}

	public Employee(String name, int document, Role role) {
		name(name);
		document(document);
		role(role);
	}

	public Employee(int id, String name, int document, Role role) {
		id(id);
		name(name);
		document(document);
		role(role);
	}

	private void validaEmploye(Employee employe) {
		if (employe == null)
			throw new IllegalArgumentException("El empleado no puede ser nulo");
	}

	public int id() {
		return id;
	}

	public void id(int id) {
		this.id = id;
	}

	public int document() {
		return document;
	}

	public void document(int document) {
		this.document = document;
	}

	public String name() {
		return name;
	}

	public void name(String name) {
		this.name = name;
	}

	public String email() {
		return email;
	}

	public void email(String email) {
		this.email = email;
	}

	public String phone() {
		return phone;
	}

	public void phone(String phone) {
		this.phone = phone;
	}

	public ImageIcon photo() {
		return photo;
	}

	public void photo(ImageIcon photo) {
		this.photo = photo;
	}

	public String gender() {
		return gender;
	}

	public void gender(String gender) {
		this.gender = gender;
	}

	public int age() {
		return age;
	}

	public void age(int age) {
		this.age = age;
	}

	public void role(Role role) {
		this.role = role;
	}

	public Role role() {
		return role;
	}

	@Override
	public String toString() {
		return name();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Employee clone() {
		Employee res = new Employee();
		res.id(id());
		res.name(name());
		res.document(document());
		res.gender(gender());
		res.age(age());
		res.email(email());
		res.phone(phone());
		res.photo(photo());
		res.role(role().clone());

		return res;
	}

}
