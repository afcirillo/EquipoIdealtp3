package controls;

import java.io.Serializable;

public class Picture implements Serializable{
	private int width;
	private int height;
	private int[] rgb;
	
	public Picture(int width, int height, int[] rgb) {
		width(width);
		height(height);
		rgb(rgb);
	}
	public int width() { return width; }
	public void width(int width) {
		if (width < 0)
			throw new IllegalArgumentException("El ancho no puede ser negativo");
		this.width = width;
	}
	public int height() { return height; }
	public void height(int height) {
		if (height < 0)
			throw new IllegalArgumentException("El alto no puede ser negativo");
		this.height = height;
	}
	public int[] rgb() { return rgb; }
	public void rgb(int[] rgb) { this.rgb = rgb; }	
}
