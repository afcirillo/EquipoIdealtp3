package controls;

public class Role {
	private int id;
	private String description;

	public Role(String description) {
		description(description);
	}

	public Role(int id, String description) {
		id(id);
		description(description);
	}

	public int id() {
		return id;
	}

	public void id(int id) {
		this.id = id;
	}

	public String description() {
		return description;
	}

	public void description(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Role clone() {
		Role res = new Role(description());
		res.id(id());
		return res;
	}

}
