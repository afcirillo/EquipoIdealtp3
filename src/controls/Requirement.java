package controls;

import java.util.ArrayList;

public class Requirement {
	private int id;
	private String description;
	private ArrayList<RequirementItem> items;
	private String status;

	public Requirement() {
		items = new ArrayList<RequirementItem>();
	}

	public Requirement(ArrayList<RequirementItem> reqList) {
		items = reqList;
	}

	public void add(RequirementItem item) {
		if (exists(item))
			return;
		items.add(item);
	}

	public boolean exists(RequirementItem item) {
		return items.contains(item);
	}

	public int count() {
		return items.size();
	}

	public String status() {
		return status;
	}

	public void status(String status) {
		this.status = status;
	}

	public ArrayList<RequirementItem> items() {
		return items;
	}

	public void items(ArrayList<RequirementItem> items) {
		this.items = items;
	}

	public int id() {
		return id;
	}

	public void id(int id) {
		this.id = id;
	}

	public String description() {
		return description;
	}

	public void description(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return description();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Requirement other = (Requirement) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Requirement clone() {
		Requirement res = new Requirement();
		res.id(id());
		res.description(description());
		for (RequirementItem item : items)
			res.items().add(new RequirementItem(item.role(), item.minimum(), item.maximum()));
		return res;
	}
}
