package controls;

import java.util.ArrayList;

import javax.swing.JProgressBar;

import controls.RequirementItem;

public class TeamGenerator extends Thread {
	private ArrayList<RequirementItem> requirements;
	private ArrayList<Employee> employees;
	private ArrayList<Conflict> conflicts;
	private double taskProgress;
	private Employee[] result;
	private int elapsedTimeInMs;
	private int correctCombinations;
	private int iterations;

	public TeamGenerator(Requirement requirement, ArrayList<Employee> employees,
			ArrayList<Conflict> incompatibilities) {
		requirements(requirement.items());
		employees(employees);
		incompatibilities(incompatibilities);
	}

	public void resolveByBacktracking() {
		long startTime = System.currentTimeMillis();
		elapsedTimeInMs(0);
		taskProgress(0);
		correctCombinations(0);
		iterations(0);
		result = new Employee[0];
		double step = 100 / employees.size();
		ArrayList<Employee> employeesOrderedByConflict = new ArrayList<Employee>();
		int[] employeeConflict = new int[employees.size()];
		for (int i = 0; i < employees.size(); i++) {
			for (Conflict conflict : conflicts) {
				employeeConflict[i] += (conflict.employee1().equals(employees.get(i)) ? 1 : 0);
				employeeConflict[i] += (conflict.employee2().equals(employees.get(i)) ? 1 : 0);
			}
		}
		for (int i = 0; i < employeeConflict.length; i++) {
			if (employeeConflict[i] > 0) {
				employeesOrderedByConflict.add(employeesOrderedByConflict.size(), employees.get(i));
				continue;
			}
			employeesOrderedByConflict.add(0, employees.get(i));

		}
		ArrayList<Employee> employeeList = new ArrayList<Employee>();

		for (Employee employee : employeesOrderedByConflict) {
			taskProgress += step;
			if (!isNecessary(employee, employeeList))
				continue;
			if (!isCompatible(employee, employeeList))
				continue;
			employeeList.add(employee);
		}
		taskProgress = 100;
		elapsedTimeInMs((int) (System.currentTimeMillis() - startTime));
		if (requirementsApproved(employeeList)) {
			result = employeeList.toArray(new Employee[0]);
			return;
		}
	}

	public void resolveByBacktracking2() {
		long startTime = System.currentTimeMillis();

		elapsedTimeInMs(0);
		taskProgress(0);
		correctCombinations(0);
		iterations(0);

		double step = 100 / Math.pow(2, employees.size());
		result = new Employee[0];
		ArrayList<Employee[]> combinationList = new ArrayList<Employee[]>();
		ArrayList<Employee> employeeList;
		int iterationsCont = 0;

		for (long i = 1; i < Math.pow(2, employees.size()); i++) {
			employeeList = new ArrayList<Employee>();

			taskProgress += step;
			String binary = Long.toBinaryString(i);
			binary = binary.replace("0", "");

			if (binary.length() < minRequirements() || binary.length() > maxRequirements())
				continue;

			iterationsCont++;
			for (int j = 0; j < employees.size(); j++) {
				if ((i & (long) Math.pow(2, j)) > 0) {
					employeeList.add(employees.get(j));
				}
			}
			if (employeesAreCompatible(employeeList) && requirementsApproved(employeeList)) {
				combinationList.add(employeeList.toArray(new Employee[0]));
			}

		}
		Employee[][] resultBruteForceMatrix = combinationList.toArray(new Employee[0][0]);
		biggestResult(resultBruteForceMatrix);

		iterations(iterationsCont);
		elapsedTimeInMs((int) (System.currentTimeMillis() - startTime));
		taskProgress(100);
		correctCombinations(resultBruteForceMatrix.length);
	}

	public void resolveByBruteForce() {
		long startTime = System.currentTimeMillis();

		elapsedTimeInMs(0);
		taskProgress(0);
		correctCombinations(0);
		iterations(0);

		double step = 100 / Math.pow(2, employees.size());
		result = new Employee[0];
		ArrayList<Employee[]> combinationList = new ArrayList<Employee[]>();
		ArrayList<Employee> employeeList;
		int iterationsCont = 0;

		for (long i = 1; i < Math.pow(2, employees.size()); i++) {
			taskProgress += step;
			employeeList = new ArrayList<Employee>();

			for (int j = 0; j < employees.size(); j++) {
				if ((i & (long) Math.pow(2, j)) > 0) {
					iterationsCont++;
					employeeList.add(employees.get(j));
				}
			}

			if (employeesAreCompatible(employeeList) && requirementsApproved(employeeList)) {
				if (employeeList.size() >= minRequirements() && employeeList.size() <= maxRequirements())
					combinationList.add(employeeList.toArray(new Employee[0]));
			}
		}
		Employee[][] resultBruteForceMatrix = combinationList.toArray(new Employee[0][0]);
		biggestResult(resultBruteForceMatrix);

		iterations(iterationsCont);
		elapsedTimeInMs((int) (System.currentTimeMillis() - startTime));
		taskProgress(100);
		correctCombinations(resultBruteForceMatrix.length);
	}

	private int minRequirements() {
		int min = 0;
		for (RequirementItem item : requirements()) {
			min += item.minimum();
		}
		return min;
	}

	private int maxRequirements() {
		int max = 0;
		for (RequirementItem item : requirements()) {
			max += item.maximum();
		}
		return max;
	}

	private void biggestResult(Employee[][] matrix) {
		int arrayLength = 0;
		Employee[] result = null;
		for (Employee[] employees : matrix) {
			if (employees.length > arrayLength) {
				arrayLength = employees.length;
				result = employees;
			} else if (employees.length == arrayLength) {
			}
		}
		this.result = result;
	}

	private boolean isNecessary(Employee employee, ArrayList<Employee> employeeList) {

		RequirementItem item;
		try {
			item = (RequirementItem) requirements.stream().filter(r -> r.role().equals(employee.role())).findFirst()
					.get();
		} catch (Exception e) {
			return false;
		}
		if (item == null)
			return false;
		int quantity = 0;
		for (Employee e : employeeList) {
			quantity += (e.role().equals(item.role()) ? 1 : 0);
		}

		return !item.validate(quantity);
	}

	private boolean isCompatible(Employee employee, ArrayList<Employee> team) {
		for (Employee employee2 : team) {
			if (employee2.equals(employee))
				continue;
			for (Conflict incompatibility : conflicts) {
				if (incompatibility.equals(new Conflict(employee, employee2)))
					return false;
			}
		}
		return true;
	}

	private boolean employeesAreCompatible(ArrayList<Employee> empleados) {
		boolean acumulador = true;
		for (Employee empleado : empleados) {
			acumulador &= isCompatible(empleado, empleados);
		}
		return acumulador;
	}

	private boolean requirementsApproved(ArrayList<Employee> empleados) {
		int quantity;
		Role role;
		boolean valid = true;

		// System.out.println("Grupo---------------------------");
		for (RequirementItem item : requirements) {
			quantity = 0;
			role = item.role();

			for (Employee employee : empleados) {
				if (employee.role().equals(role)) {
					quantity++;
				}
			}
			valid &= item.validate(quantity);
		}
		return valid;
	}

	public int maxIterations() {
		return 0;
	}

	public ArrayList<RequirementItem> requirements() {
		return requirements;
	}

	public void requirements(ArrayList<RequirementItem> requirements) {
		this.requirements = requirements;
	}

	public ArrayList<Employee> employees() {
		return employees;
	}

	public void employees(ArrayList<Employee> employees) {
		this.employees = employees;
	}

	public ArrayList<Conflict> incompatibilities() {
		return conflicts;
	}

	public void incompatibilities(ArrayList<Conflict> incompatibilities) {
		this.conflicts = incompatibilities;
	}

	@Override
	public String toString() {
		String res = "";
		for (Employee employee : employees) {
			res += employee + "\n";
		}
		return res;
	}

	public Employee[] result() {
		return result;
	}

	public void result(Employee[] result) {
		this.result = result;
	}

	public double taskProgress() {
		return taskProgress;
	}

	protected void taskProgress(int taskProgress) {
		this.taskProgress = taskProgress;
	}

	public int elapsedTimeInMs() {
		return elapsedTimeInMs;
	}

	protected void elapsedTimeInMs(int elapsedTimeInMs) {
		this.elapsedTimeInMs = elapsedTimeInMs;
	}

	public int correctCombinations() {
		return correctCombinations;
	}

	protected void correctCombinations(int correctCombinations) {
		this.correctCombinations = correctCombinations;
	}

	public int iterations() {
		return iterations;
	}

	protected void iterations(int iterations) {
		this.iterations = iterations;
	}
}
