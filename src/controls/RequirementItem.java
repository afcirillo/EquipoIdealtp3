package controls;

import java.io.Serializable;

import controls.RequirementItem;

public class RequirementItem implements Serializable{
	private Role role;
	private int minimum;
	private int maximum;

	public RequirementItem(Role role, int minimum, int maximum) {
		role(role);
		minimum(minimum);
		maximum(maximum);
	}

	public int minimum() {
		return minimum;
	}

	public void minimum(int minimum) {
		this.minimum = minimum;
	}

	public int maximum() {
		return maximum;
	}

	public void maximum(int maximum) {
		this.maximum = maximum;
	}

	public Role role() {
		return role;
	}

	public void role(Role role) {
		this.role = role;
	}
	
	public boolean validate(int quantity) {
		return (minimum <= quantity && quantity <= maximum());
	}
	
	@Override
	public String toString() {
		return (role.description() + " (min = " + minimum + " - max = " + maximum + ")");
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequirementItem other = (RequirementItem) obj;
		if (!role.equals(other.role())) 
				return false;
		return true;
	}
	
	public RequirementItem clone() {
		return new RequirementItem(role(), minimum(), maximum());
	}
	
}
